/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('admin-lte');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))



Vue.component('generic-table', require('./components/tools/GenericTableComponent.vue').default);
///Vue.component('table-advanced', require('./components/AdvancedTableComponent.vue').default);

Vue.component('form-component', require('./components/tools/FormComponent.vue').default);

Vue.component('generic-modal', require('./components/tools/GenericModalComponent').default);
// navcomponent will add to container

//container holds the active pages.

Vue.component('container-component', require('./components/container/ContainerComponent').default);

//container top nav holds the active pages navbar.

Vue.component('container-top-nav-component', require('./components/container/TopNavContainerComponent').default);

//container side nav triggers adding of page to container.

Vue.component('nav-container-component', require('./components/container/NavContainerComponent').default);


// page are created by container
Vue.component('page-component', require('./components/section/PageComponent.vue').default);

// partial datagrid for requests and display

Vue.component('partial-datagrid-component', require('./components/section/PartialDatagridComponent.vue').default);

// Crud helper components

Vue.component('belongs-to-many-component', require('./components/crud/BelongsToManyComponent.vue').default);

Vue.component('has-many-component', require('./components/crud/HasManyComponent.vue').default);

// particals for page.
Vue.component('layout-breadcrumb', require('./components/layout/BreadcrumbComponent.vue').default);




/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */





 import BootstrapVue from 'bootstrap-vue' //Importing
 Vue.use(BootstrapVue)

import VueEvents from 'vue-events'
Vue.use(VueEvents)


const app = new Vue({
    el: '#app',
});

