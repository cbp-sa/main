@extends(config('template.layout'))
@section('content')


<div class="row">
    <div class="col-md-12">
        <generic-table
        :grid-Title="userDataGridData.title"
        :grid-Data="testData"
        :columns="userDataGridData.gridColumns"
        :filter-key="userDataGridData.filter"
        :grouped-key="userDataGridData.groupBy"

        ></generic-table>

    </div>
</div>


@endsection
