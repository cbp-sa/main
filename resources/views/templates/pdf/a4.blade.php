<!DOCTYPE html>
<html>
<style>



    @page {
        margin-top:1em;
        margin-left:1em;
        margin-right:1em;
        margin-bottom:6em;
    }
    .oddRow {
        background: #EEE;
    }
    body {

        font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
        font-size: 1em;
        white-space:normal;
        display:block;
        word-wrap:break-word;
        margin:0px;
        padding:0px;
        clear:both;
        }

        .pad_10{
            padding:10px;
        }

        .grid-0{
            width:75px;
        }

        .grid-1{
            width:100px;
        }
        .grid-2{
            width:150px;
        }
        .grid-3{
            width:250px;
        }
        .grid-4{
            margin:0px;
            width:300px;

        }
        .grid-6{
            margin:0px;
            width:350px;

        }

        .grid-12{
            margin:0px;
            width:900px;
            display:block;
        }

    .left {
        text-align:left;
    }
    .right{
        text-align:right;
    }
    .center {
        text-align:center;
    }
    .list{
        width: 99%;
        margin-top: 1em;
    }
    .pad-2{
        padding:2px;
    }
    .pad-5{
        padding:5px;
    }
    .field{

        padding-left: .5em;

        vertical-align:middle;
    }
    table {
        border-spacing:0px;
    }

    table {
        -fs-table-paginate: paginate;
     }


    tr.label th{
        color:#fff;
        background-color:{{config('doctemplate.background')}};
        font-size:12px;
        border:1px solid {{config('doctemplate.background')}};
        text-transform:capitalize;
    }
    tr.border{


            border:1px solid {{config('doctemplate.background')}};

    }
    tr.border td{
            border:1px solid {{config('doctemplate.background')}};

    }


    .highlight {
        color:#fff;
        background-color:{{config('doctemplate.background')}};
    }

    th{

        border-right: solid 1px #fff !important;
        font-size:11px !important;
    }

    th:last-child{
        border-right: 0 !important;
    }

    .footer {
        bottom: 0px;
        font-size:10px;
    }
    .pagenum:before {
        content: counter(page);
    }

    hr {
        border-bottom:solid 1px ;
        border-top:none;
    }

    .sm {
        font-size: 8pt;
    }

    .md {
        font-size: 10pt;
    }
    .xl{
        font-size:24pt;
    }

    .lg{
        font-size: 16pt;
    }
    .xs{
        font-size:6px; margin:0px; padding:2px;
    }
    .no-border{
        border:none;
    }
    .bold {
        font-weight: :bold;
    }


    @media print {
        thead {
            display: table-header-group;
        }
    }


</style>


    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </head>
    <body >



        <p class="left" >{{config('app.company.name')}} shall not be liable for any damage or loss, whether direct or indirect, arising out of the use of or the omission to use the statement made in response to the enquiry made herein or for any consequential damages, loss of profit or special damages arising out of the issuing of that statement or any use thereof.</p>
    </body>
</html>
