<!DOCTYPE html>
<html>
<style>



    @page {
        margin-top:1em;
        margin-left:1em;
        margin-right:1em;
        margin-bottom:6em;
    }
    .oddRow {
        background: #EEE;
    }
    body {

    font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
    font-size: 1em;
    white-space:normal;
    display:block;
    word-wrap:break-word;
    margin:0px;
    padding:0px;
    clear:both;
    }

    caption{
        background: #3F474E;
    color: #FFFFFF;
    font-size: 14px;
    font-weight: bold;
    padding: 5px 5px;
    text-align: left;
    text-transform: uppercase;
    height: 25px;

    }

    .pad_10{
        padding:10px;
    }

    .grid-0{
        width:75px;
    }

    .grid-1{
        width:100px;
    }
    .grid-2{
        width:150px;
    }
    .grid-3{
        width:250px;
    }
    .grid-4{
        margin:0px;
        width:300px;

    }
    .grid-6{
        margin:0px;
        width:350px;

    }

    .grid-12{
        margin:0px;
        width:900px;
        display:block;
    }

    .left {
        text-align:left;
    }
    .right{
        text-align:right;
    }
    .center {
        text-align:center;
    }
    .list{
        width: 99%;
        margin-top: 1em;
    }
    .pad-2{
        padding:2px;
    }
    .pad-5{
        padding:5px;
    }
    .field{

        padding-left: .5em;

        vertical-align:middle;
    }



    .highlight {
        color:#fff;
        background-color:{{config('doctemplate.background')}};
    }

    .footer {
        bottom: 0px;
        font-size:10px;
    }
    .pagenum:before {
        content: counter(page);
    }

    hr {
        border-bottom:solid 1px ;
        border-top:none;
    }

    .sm {
        font-size: 8pt;
    }

    .md {
        font-size: 10pt;
    }
    .xl{
        font-size:24pt;
    }

    .lg{
        font-size: 16pt;
    }
    .xs{
        font-size:6px; margin:0px; padding:2px;
    }
    .no-border{
        border:none;
    }
    .bold {
        font-weight: :bold;
    }


    @media print {
        thead {
            display: table-header-group;
        }
    }


</style>

<style>

    .table {
      width: 100%;
      max-width: 100%;
      margin-bottom: 1rem;
      border-spacing:0px;
    }

    .table th,
    .table td {
      padding: 0.75rem;
      vertical-align: top;
      border-top: 1px solid #eceeef;
      text-align:left;
    }

    .table thead th {
      vertical-align: bottom;
      border-bottom: 2px solid #eceeef;
    }

    .table tbody + tbody {
      border-top: 2px solid #eceeef;
    }

    .table .table {
      background-color: #fff;
    }

    .table-sm th,
    .table-sm td {
        font-size: 6.5pt;
        letter-spacing: 1pt;
        padding: 0.3rem;

    }

    .table-bordered {
      border: 1px solid #eceeef;
    }

    .table-bordered th,
    .table-bordered td {
      border: 1px solid #eceeef;
    }

    .table-bordered thead th,
    .table-bordered thead td {
      border-bottom-width: 2px;
    }

    .table-striped tbody tr:nth-of-type(odd) {
      background-color: rgba(0, 0, 0, 0.05);
    }

    .table-hover tbody tr:hover {
      background-color: rgba(0, 0, 0, 0.075);
    }

    .table-active,
    .table-active > th,
    .table-active > td {
      background-color: rgba(0, 0, 0, 0.075);
    }

    .table-hover .table-active:hover {
      background-color: rgba(0, 0, 0, 0.075);
    }

    .table-hover .table-active:hover > td,
    .table-hover .table-active:hover > th {
      background-color: rgba(0, 0, 0, 0.075);
    }

    .table-success,
    .table-success > th,
    .table-success > td {
      background-color: #dff0d8;
    }

    .table-hover .table-success:hover {
      background-color: #d0e9c6;
    }

    .table-hover .table-success:hover > td,
    .table-hover .table-success:hover > th {
      background-color: #d0e9c6;
    }

    .table-info,
    .table-info > th,
    .table-info > td {
      background-color: #d9edf7;
    }

    .table-hover .table-info:hover {
      background-color: #c4e3f3;
    }

    .table-hover .table-info:hover > td,
    .table-hover .table-info:hover > th {
      background-color: #c4e3f3;
    }

    .table-warning,
    .table-warning > th,
    .table-warning > td {
      background-color: #fcf8e3;
    }

    .table-hover .table-warning:hover {
      background-color: #faf2cc;
    }

    .table-hover .table-warning:hover > td,
    .table-hover .table-warning:hover > th {
      background-color: #faf2cc;
    }

    .table-danger,
    .table-danger > th,
    .table-danger > td {
      background-color: #f2dede;
    }

    .table-hover .table-danger:hover {
      background-color: #ebcccc;
    }

    .table-hover .table-danger:hover > td,
    .table-hover .table-danger:hover > th {
      background-color: #ebcccc;
    }

    .thead-inverse th {
      color: #fff;
      background-color: #292b2c;
    }

    .thead-default th {
      color: #464a4c;
      background-color: #eceeef;
    }

    .table-inverse {
      color: #fff;
      background-color: #292b2c;
    }

    .table-inverse th,
    .table-inverse td,
    .table-inverse thead th {
      border-color: #fff;
    }

    .table-inverse.table-bordered {
      border: 0;
    }

    .table-responsive {
      display: block;
      width: 100%;
      overflow-x: auto;
      -ms-overflow-style: -ms-autohiding-scrollbar;
    }

    .table-responsive.table-bordered {
      border: 0;
    }

    </style>


    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </head>
    <body >

        {{-- start of add tables --}}
        @foreach($model->module as $module)

            <h3>{{strtoupper($model->name. ' REPORT' . $module->name)}}</h3>


            @foreach($module->section as $section)
                <caption>{{$section->name}}</caption>
                @table($section['data']) @endtable
            @endforeach
        @endforeach
        {{-- end of add tables --}}

        {{-- start of disclamer --}}
        <p class="left" >{{config('app.company.name')}} shall not be liable for any damage or loss, whether direct or indirect, arising out of the use of or the omission to use the statement made in response to the enquiry made herein or for any consequential damages, loss of profit or special damages arising out of the issuing of that statement or any use thereof.</p>
        {{-- end of disclamer --}}
    </body>
</html>
