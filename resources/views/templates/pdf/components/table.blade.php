
@php
$totalColm=0;
@endphp

@foreach($data as $index =>  $item)

<table class="table table-sm table-striped table-bordered" >
    <thead  class="thead-light">
        <tr>
            @foreach(array_keys($item->first()) as $value)
                <th >{{preg_replace('/(?<!\ )[A-Z]/', ' $0', $value) }}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach($item as $value)
            <tr>
                @foreach($value as $prop)
                <td>{{$prop}}</td>
                @endforeach
            </tr>
        @endforeach
    </tbody>

</table>
@endforeach
