<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style type="text/css">
        body {
            font-family: Arial, Helvetica, sans-serif;
            margin: 0;
            padding: 0;
            line-height: 2;
            font-size: 8pt;
            letter-spacing: 1pt;
            height: 40mm; /* set it to your bottom margin */
        }

        .footer {
            position: absolute;
            bottom: 0;
            left: 0;
            right: 0;
        }
        .right{
            text-align: right;
        }
    </style>
</head>

<script>
    var pdfInfo = {};
    var x = document.location.search.substring(1).split('&');
    for (var i in x) { var z = x[i].split('=',2); pdfInfo[z[0]] = unescape(z[1]); }
    function getPdfInfo() {
        var page = pdfInfo.page || 1;
        var pageCount = pdfInfo.topage || 1;
        document.getElementById('pdfkit_page_current').textContent = page;
        document.getElementById('pdfkit_page_count').textContent = pageCount;
    }

</script>


<body onload="getPdfInfo()">
    <div class="footer">
        <hr>
        <table border="0" width="100%">
            <tr>
                <td width="70%" border="0" >
                    <p class="left" >
                    Produced by {{config('app.name')}}.  &copy; {{config('app.company.name')}}, {{date('Y')}} All Rights Reserved.<br>
                    Reg No:{{config('app.company.reg_no')}} | NCR Reg No: {{config('app.company.ncr_reg_no')}}  | VAT No: {{config('app.company.vat_no')}}
                    </p>
                </td>
                <td width="30%" border="0" class="right"  valign="middle">
                    Page <span id="pdfkit_page_current"></span> of <span id="pdfkit_page_count"></span><br>
                    <span class="sm">{{date("F j, Y, g:i a")}}</span>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
