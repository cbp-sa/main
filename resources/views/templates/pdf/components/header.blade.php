<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style type="text/css">
        body {
            font-family: Arial, Helvetica, sans-serif;
            margin: 0;
            padding: 0;
            line-height: 2;
            font-size: 8pt;
            height: 40mm; /* set it to your bottom margin */
        }

        .header {
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
        }
        .right{
            text-align: right;
        }
    </style>
</head>

<script>
    var pdfInfo = {};
    var x = document.location.search.substring(1).split('&');
    for (var i in x) { var z = x[i].split('=',2); pdfInfo[z[0]] = unescape(z[1]); }
    function getPdfInfo() {
        var page = pdfInfo.page || 1;
        var pageCount = pdfInfo.topage || 1;
        document.getElementById('pdfkit_page_current').textContent = page;
        document.getElementById('pdfkit_page_count').textContent = pageCount;
    }

</script>


<body onload="getPdfInfo()">
    <div class="header">
        <table border="0"  width="100%"  >
            <tr>
                <th class="grid-4" width="50%">
                <img src='{{base_path('public').config('app.company.logo')}}' style='float:left' width="180"  >
                </th>


                <th  class="right grid-4" width="50%">
                    Tel: {{config('app.company.tell')}}<br>

                    Email:{{config('app.company.email')}}<br>

                    Reg No:{{config('app.company.reg_no')}}<br>
                </th>
            </tr>

        </table>
        <hr>
    </div>
</body>
</html>
