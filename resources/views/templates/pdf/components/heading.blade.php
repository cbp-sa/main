<!DOCTYPE html>

<div class="header">
    <table border="0"  width="100%"  >
        <tr>
            <th class="grid-4" width="33%">
                <img src='{{config('app.company.logo')}}' width="120"  >
            </th>

            <th class="grid-4 center" width="33%">
                <h1 class="xl" style="font-family:Arial, Helvetica, sans-serif">Title</h1>
            </th>

            <th  class="right grid-4" width="33%">
                Tel: {{config('app.company.tell')}}<br>

                Email:{{config('app.company.email')}}<br>

                Reg No:{{config('app.company.reg_no')}}<br>
            </th>
        </tr>

    </table>
    <hr>
    <table border="0" class="sm"  >
        <tr>
            <td>
                {{config('app.company.name')}}
            </td>
            <td></td>
        </tr>

    </table>
</div>
