<!DOCTYPE html>
<html>
<head>
<style>
body {
    margin:     0;
    padding:    0;
    width:      21cm;
    height:     29.7cm;
}

/* Printable area */
#print-area {
    position:   relative;
    top:        0cm;
    left:       0cm;
    width:      24cm;
    height:     33.6cm;

    font-size:      10px;
    font-family:    Arial;
}

#header {
    height:     3cm;

    background: #ccc;
}
#footer {
    position:   absolute;
    bottom:     0;
    width:      100%;
    height:     3cm;

    background: #ccc;
}
</style>

<script>
    var pdfInfo = {};
    var x = document.location.search.substring(1).split('&');
    for (var i in x) { var z = x[i].split('=',2); pdfInfo[z[0]] = unescape(z[1]); }
    function getPdfInfo() {
        var page = pdfInfo.page || 1;
        var pageCount = pdfInfo.topage || 1;
        document.getElementById('pdfkit_page_current').textContent = page;
        document.getElementById('pdfkit_page_count').textContent = pageCount;
    }

</script>

</head>
<body onload="getPdfInfo()">

    <div id="print-area">
        <div id="header">
            This is an example header.
        </div>
        <div id="content">
            <h1>Demo</h1>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <div style = "display:block; clear:both; page-break-after:always;"></div>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>

            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
            <p>This is example content</p>
        </div>
        <div id="footer">
            This is an example footer.
            Page <span id="pdfkit_page_current"></span> of <span id="pdfkit_page_count"></span>
        </div>
    </div>

</body>
</html>
