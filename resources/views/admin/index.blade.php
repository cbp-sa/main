@extends('admin.layouts.master')
@section('content')


<section class="container-fluid">
    <div class="container-fluid">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Dashboard</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <div class="btn-group mr-2">
                    <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>
                </div>
            </div>
        </div>
    </div>


    <div class="content-body">
        <div class="container-fluid">
            <div class="row ">
                <div class="col-md-2">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Module Management</h5>
                        </div>
                        <div class="card-body">

                            <p class="card-text">Configure products and link sections.</p>
                            <a href="#" class="card-link">Products</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
