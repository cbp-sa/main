@extends('admin.layouts.master')
@section('content')



<div class="container-fluid">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ ucfirst($title) }} Edit - {{$data->name}}</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
            <a href="{{ url()->previous() }}" class="btn btn-sm btn-outline-secondary" ><i class="fas fa-arrow-left"></i> Back</a>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    <div class="container-fluid">
        <div class="row ">
            <div class="col-md-12">

                <div class="card">
                    <div class="card-body">

                        <form-component :data="{{$data->toJson()}}" :fields="{{$data->getFieldsConfig()->toJson()}}"  :route="'{!!CrudHelperBaseUrl().'update'. '/'. $data->uniqueKey!!}'" ></form-component>

                    </div>
                </div>
                {{-- Build up the  gui for relations maped in the model , see ModelHelper, RelationshipsHelper and the CrudController  --}}

                {!!json_encode($data->relationships() )!!}


                @foreach($data->relationships() as $key => $item)
                    @switch($item['type'])
                        {{-- A many to many relationship will only require you to attach the associate model to form the relationship; --}}
                        @case('BelongsToMany')
                        @php $tmp = "\\". $item['model'] @endphp
                            @crud_belongstomany([
                                'parent' =>$data->uniqueKey,
                                'model' => $key,
                                'route' => $route
                            ])
                            @endcrud_belongstomany
                        @break

                        {{-- Has Many will require you to create a new model and then attach; --}}

                        @case('HasMany')
                        @php $tmp = "\\". $item['model'] @endphp

                            @crud_hasmany([
                                'parent' =>$data->uniqueKey,
                                'model' => $key,
                                'route' => $route,
                                'fieldsConfig' => $data->getFieldsConfig()
                            ])
                            @endcrud_hasmany
                        @break

                        @default

                    @endswitch
                @endforeach


            </div>
        </div>
    </div>
</div>


@endsection
