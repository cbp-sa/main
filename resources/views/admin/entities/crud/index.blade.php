@extends('admin.layouts.master')
@section('content')


<div class="container-fluid">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ ucfirst($title) }}</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <button type="button" class="btn btn-sm btn-outline-secondary"  data-toggle="collapse" data-target="#crudCreate" >Create</button>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    <div class="container-fluid">
        <div class="row ">
            <div class="col-md-12">
                <div class="card">
                    <div id="crudCreate" class="collapse" aria-labelledby="headingOne">
                        <div class="card-body">
                            <form-component :fields="{{$fields->toJson()}}" :data="{}" :route="'{!!CrudHelperBaseUrl().'create'!!}'" ></form-component>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">

                        @crud_table(['data' => $data, 'keys' => collect($data->first())->keys(), 'route' => $route ])
                        @endcrud_table

                    </div>
                    <div class="card-footer">
                        {{$data->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




@endsection
