<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <link rel="stylesheet" href="/css/app.css">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>

            body{
                display: -ms-flexbox;
                display: flex;
                -ms-flex-align: center;
                align-items: center;
                padding-top: 40px;
                padding-bottom: 40px;
                background-color: #f5f5f5;
            }
            .form-signin {
                width: 100%;
                max-width: 330px;
                padding: 15px;
                margin: auto;
            }


            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

        </style>
    </head>
    <body class="full-height">
        <form class="form-signin" method="POST" action="{{ route('login') }}">
            @csrf
            <h1 class="h1 mb-1 font-weight-normal text-center">{{config('app.name')}}</h1>
            <hr>
            <h1 class="h3 mb-3 font-weight-normal text-center">Please sign in</h1>
            <hr>
            <label for="accountNumber" class="sr-only">Account no:</label>
            <input type="text" name="accountNumber" id="accountNumber" class="form-control" placeholder="Account Number" required="" autofocus="">
            <label for="userCode" class="sr-only">User Code:</label>
            <input type="text" name="userCode" id="inputPassword" class="form-control" placeholder="User Code" required="">
            <label for="inputPassword" class="sr-only">Password</label>
            <input type="password"  name="password" id="inputPassword" class="form-control" placeholder="Password" required="">
            <div class="checkbox mb-3">
                <label>
                    <input type="checkbox" value="remember-me"> Remember me
                </label>
            </div>

            <button type="submit" class="btn btn-lg btn-primary btn-block">
                {{ __('Login') }}
            </button>

            @if (Route::has('password.request'))
                <a class="btn btn-link" href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                </a>
            @endif


            <p class="mt-2 mb-3 text-muted">Copyright © 1981 - {{Carbon\Carbon::now()->format('Y')}}. All Rights Reserved.</p>
            <p class="mt-2 mb-3 text-right text-muted">Tel: {{config('app.company.tell')}} Address: Bureau place, Turnberry Office Park, 48 Grosvenor Road, Bryanston, 2021</p>
        </form>
    </body>
</html>
