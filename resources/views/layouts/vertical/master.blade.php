<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content={{csrf_token()}}>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <title>{{config('app.name')}}</title>
    <link rel="stylesheet" href="/css/app.css">

</head>


<body class="hold-transition sidebar-mini  skin-yellow">
    @guest
        @yield('content')
    @else
    <div class="wrapper" id="app">
        @include('layouts.vertical.include.header')
        @include('layouts.vertical.include.sidebar')
        <div class="content-wrapper">
            @yield('content')
        </div>
        @include('layouts.vertical.include.footer')
    </div>
    <!-- ./wrapper -->
    @endguest
    @yield('javascript')

</body>

</html>
