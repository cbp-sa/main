<footer class="main-footer">
    <small>
        <strong>{{date('Y')}} &copy; Consumer Profile Bureau. All Rights Reserved | Reg: 1981/007624/07 - NCR: NCRCB2</strong>
        | <a href="{{route('admin-index')}}">Admin Dashboard</a> | <a href="{{route('switch-layout','vertical')}}">Vertical</a> - <a href="{{route('switch-layout', 'horizontal')}}">Horizontal</a>
        | <a href="{{route('switch-company','cpb')}}">CPB</a> - <a href="{{route('switch-company', 'cc')}}">CC</a>
    </small>
</footer>
