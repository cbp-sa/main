

<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">

    <a href="home" class="brand-link">
    <img src="{{config('app.badge')}}"
    alt="{{config('app.name')}}"
               class="brand-image "
    >
    <span class="brand-text font-weight-light">{{config('app.name')}}</span>
        </a>

      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="pull-left image">
                <img src="\images\general\avatar\man-1.svg" class="img-circle" alt="User Image">
              </div>
          <div class="info">
            <a href="#" class="d-block">Login: {{auth()->user()->accountNumber!=null ? auth()->user()->accountNumber : "Administrator"}} </a>
          </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item has-treeview {!! classActivePath(1,'home') !!}">
              <a href="{!! route('home') !!}" class="nav-link {!! classActiveSegment(1, 'home') !!}">
                <i class="nav-icon fas fa-tachometer-alt"></i><p>Home</p>
              </a>
            </li>

            <nav-container-component :icons='true'></nav-container-component>


            {{-- <li class="nav-item">
                <a href="{{route('sections','Directory' )}}" class="nav-link">
                    <i class="nav-icon fa fa-th"></i>
                    <p>
                      Directory
                      <span class="right badge badge-danger">New</span>
                    </p>
                </a>
            </li> --}}


            {{-- <li class="nav-item has-treeview">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-pie-chart"></i>
                <p>
                  Dropdown
                  <i class="right fa fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="#" class="nav-link">

                    <p>Demo</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="#" class="nav-link">

                    <p>Demo</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="#" class="nav-link">

                    <p>Demo</p>
                  </a>
                </li>
              </ul>
            </li> --}}
            <li class="nav-item">
              <a href="{{route('logout')}}" class="nav-link"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="fas fa-sign-out-alt"></i> Logout
                <p>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
              </form>

                </p>
              </a>
            </li>

          </ul>
        </nav>
        <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
    </aside>
