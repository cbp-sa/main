<footer class="font-weight-light text-center">
    <small>
        <strong>{{date('Y')}} &copy; {{config('app.company.name')}}. All Rights Reserved<br>Reg: {{config('app.company.reg_no')}} - NCR: {{config('app.company.ncr_reg_no')}}</strong>
        | <a href="{{route('admin-index')}}">Admin Dashboard</a>  |  <a href="{{route('switch-layout','vertical')}}">Vertical</a> - <a href="{{route('switch-layout', 'horizontal')}}">Horizontal</a>
    | <a href="{{route('switch-company','cpb')}}">CPB</a> - <a href="{{route('switch-company', 'cc')}}">CC</a> | <a href="{{route('test')}}">Test</a>

    </small>
</footer>
