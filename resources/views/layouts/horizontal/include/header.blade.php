
    <nav class="navbar navbar-expand-sm navbar-dark bg-dark">
        <div class="container">
        {{-- <a class="navbar-brand" href="#">
        <img src="{{config('app.badge')}}"
        alt="{{config('app.name')}}"
                class="brand-image"
        >
        {{config('app.name')}}</a> --}}

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample03" aria-controls="navbarsExample03" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExample03">
            <ul class="navbar-nav">
                <li class="nav-item {!! classActivePath(1,'home') !!}">
                    <a href="{!! route('home') !!}" class="nav-link {!! classActiveSegment(1, 'home') !!}">
                        Home
                    </a>
                </li>
                <li class="nav-item has-treeview {!! classActivePath(2,'Directory') !!}">
                    <a href="{!! route('sections', 'Directory') !!}" class="nav-link {!! classActiveSegment(1, 'home') !!}">
                        Directory
                    </a>
                </li>
            </ul>
        </div>
        </div>
    </nav>

  <div class="container white p-0">
      <div class="row m-0 p-0">
          <div class="col-md-6 m-0 p-0">
            <img src="{{config('company.logo')}}">
          </div>

          <div class="col-md-6 m-0 text-right">
            <p><strong>Subscriber Name:</strong> {{auth()->user()->accountNumber!=null ? auth()->user()->accountNumber : "Administrator"}}
            <br><strong>Permissible Purpose:</strong> Internal Use</p>

            <div class="btn-group" role="group" aria-label="Basic example">
                <button type="button" class="btn btn-default btn-nostyle">
                    <span class="badge badge-pill badge-primary align-pill">4</span>
                    <i class="fa fa-envelope mr-2"></i>
                </button>
                <button type="button" class="btn btn-default btn-nostyle">
                    <span class="badge badge-pill badge-primary align-pill" >3</span>
                    <i class="fa fa-file mr-2"></i>
                </button>

              </div>
        </div>
      </div>
  </div>



</nav>
<!-- /.navbar -->

