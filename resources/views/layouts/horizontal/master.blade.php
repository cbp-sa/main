<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content={{csrf_token()}}>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <title>Laravel Starter</title>
    <link rel="stylesheet" href="/css/app.css">
    <style>
        body{
            background-image: url('{{config('app.background')}}') !important;
            background-size: cover;
        }

        section{
            padding-bottom: 2px;
        }

        .white{
            background-color: #fff;

        }
        .roundit{
            border-bottom-right-radius: 5px;
            border-bottom-left-radius: 5px;
        }
        nav .nav-item:hover{
            background-color: gray;
            color:#fff;
        }
        nav .nav-item a:{
            color:#BFBFBF;
        }

        footer{
            color:#fff;
        }

        .align-pill{
            position: relative;
            top: -10px;
            left: 10px;
        }

        .btn-nostyle{
            border: none !important;
            background: none !important;
        }
        .btn-nostyle:focus{
            border: none !important;
            background: none !important;
            box-shadow:none;
        }

        .btn .badge {

            top: -10px !important;
        }

        body.modal-open
        {
            margin-right: 0!important;
            overflow: hidden;
        }
        .modal-open .navbar, .modal-open .navbar
        {
            margin-right: 0!important;
        }

        .modal{
            color:#212529 !important;
        }

    </style>
</head>

<body>
    @guest
        @yield('content')
    @else
    <div  id="app">
        @include('layouts.horizontal.include.header')
        <div class="container white roundit" id="app">
            @yield('content')
        </div>
        @include('layouts.horizontal.include.footer')
    </div>
    @endguest

    @yield('javascript')

</body>

</html>
