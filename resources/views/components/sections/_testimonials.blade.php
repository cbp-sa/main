<section class="testimonials text-center bg-light">
    <div class="container">
      <h2 class="mb-5">What people are saying...</h2>
      <div class="row">
        <div class="col-lg-4">
          <div class="testimonial-item mx-auto mb-5 mb-lg-0">
            <img class="img-fluid rounded-circle mb-3" src="images/general/avatar/girl-1.svg" alt="">
            <h5>Maraai Bason.</h5>
            <p class="font-weight-light mb-0">"This is fantastic! Thanks so much guys!"</p>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="testimonial-item mx-auto mb-5 mb-lg-0">
            <img class="img-fluid rounded-circle mb-3" src="images/general/avatar/man-1.svg" alt="">
            <h5>Frikkie Wessels.</h5>
            <p class="font-weight-light mb-0">"Analytical Services that match no other :)."</p>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="testimonial-item mx-auto mb-5 mb-lg-0">
            <img class="img-fluid rounded-circle mb-3" src="images/general/avatar/man-3.svg" alt="">
            <h5>Sarah Westend.</h5>
            <p class="font-weight-light mb-0">"Thanks so much for making these resources available to us!"</p>
          </div>
        </div>
      </div>
    </div>
  </section>