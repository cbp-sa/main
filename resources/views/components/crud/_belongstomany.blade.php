<div class="card">
    <div class="card-body">
        <belongs-to-many-component
        :route="'{{$route}}'"
        :parent="'{{$parent}}'"
        :model="'{{$model}}'"
        >
        </belongs-to-many-component>
    </div>
</div>
