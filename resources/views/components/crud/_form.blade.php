{{ Form::model($data, ['route' => ['admin-product-update', $data->uniqueKey]])}}

@forelse ($data->toArray() as $key=>$item)

    @if(!in_array($key,['id','uniqueKey','created_at', 'updated_at', 'deleted_at']))
        @switch($item)

            @case(is_string($item))


                <div class="form-group">
                    {{ Form::label($key, ucfirst($key).':')}}
                    {{ Form::text($key, null ,['class' => 'form-control'])}}
                </div>

            @break

            @case(is_object($item))

            <div class="form-group">
                {{ Form::label($key, ucfirst($key).':')}}
                {{ Form::select($key, [] ,['class' => 'form-control'])}}
            </div>



            @break

            @default

                <div class="form-group">
                    {{ Form::label($key, ucfirst($key).':')}}
                    {{ Form::text($key,  $item ,['class' => 'form-control'])}}
                </div>

        @endswitch
    @endif
@endforeach


{{ Form::submit('Update', array('class' => 'btn btn-primary')) }}


{{ Form::close() }}
