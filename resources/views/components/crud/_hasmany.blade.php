<div class="card">
    <div class="card-body">
        {{-- Loads the vue component --}}
        <has-many-component
        :route="'{{$route}}'"
        :parent="'{{$parent}}'"
        :model="'{{$model}}'"
        :fields="{{$fieldsConfig}}">
        </has-many-component>
    </div>
</div>
