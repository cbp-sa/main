<div class="table-responsive-sm">
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                @foreach($keys as $item)
                <th>{{str_replace('_', ' ',ucfirst($item), )}}</th>
                @endforeach
                <th colspan="2">Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $item)
                <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    @foreach($keys as $value)
                        <td>{{$item[$value]}}</td>
                    @endforeach
                    <td><a href="{{route('admin-' . $route. '-edit', $item['uniqueKey'])}}" class="btn btn-sm btn-warning" ><i class="fas fa-edit"></i> Edit</a></td>
                    <td><button class="btn btn-sm btn-danger">
                        <i class="fas fa-trash-alt"></i> Remove</button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
