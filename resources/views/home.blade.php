@extends(config('template.layout'))
@section('content')


<div class="row">
    <div class="col-md-12">
        @masthead
        @endmasthead

        @features
        @endfeatures

        @showcase
        @endshowcase

        @calltoaction
        @endcalltoaction

        @testimonials
        @endtestimonials
    </div>
</div>


@endsection
