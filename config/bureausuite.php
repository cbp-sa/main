<?php

return [
    'api_uri' => env('REST_SERVER', 'http://bsapidev.cpbonline.co.za'),
    'bureau_name' => env('REST_BUREAU_NAME', "APITEST"),
    'vendor' => env('REST_VENDOR','BureauHouse'),
    'components' => [
        'token' => [
            'uri' => '/token/token',
            'module' => 'Integration',
            'form_params' => [
                ''
            ]
        ],
        'validate' => [

            'uri' => 'token/validate',
            'module' => 'Integration',
            'form_params' => [
                ''
            ]

        ]

    ]

];
