<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFavModuleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fav_module', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('userCode');
            $table->bigInteger('module_id')->unsigned();
            $table->bigInteger('module_use_count');
            $table->timestamps();

            $table->foreign('module_id')->references('id')->on('module');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fav_module');
    }
}
