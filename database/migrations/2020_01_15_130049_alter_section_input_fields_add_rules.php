<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSectionInputFieldsAddRules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('section_input', function (Blueprint $table) {
            $table->string('label')->after('description');
            $table->string('rules')->nullable()->after('is_required');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('section_input', function (Blueprint $table) {
            $table->dropColumn(['rules','label']);
        });
    }
}
