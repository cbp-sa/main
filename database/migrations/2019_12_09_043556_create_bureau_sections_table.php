<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBureauSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('product', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->uuid('uniqueKey');
            $table->string('icon');
            $table->string('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });


        Schema::create('module', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->uuid('uniqueKey');
            $table->string('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('module_product', function(Blueprint $table){

            $table->bigInteger('module_id')->unsigned();
            $table->bigInteger('product_id')->unsigned();

            $table->foreign('module_id')->references('id')->on('module');
            $table->foreign('product_id')->references('id')->on('product');

        });

        Schema::create('module_input', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('placeholder');
            $table->string('type');
            $table->string('default_value')->nullable();
            $table->bigInteger('module_id')->unsigned()->nullable();
            $table->boolean('is_required')->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('module_id')->references('id')->on('module');
        });

        Schema::create('section', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->uuid('uniqueKey');
            $table->string('description');
            $table->string('url');
            $table->string('callingModule');
            $table->boolean('is_enabled');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('section_input', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('placeholder');
            $table->string('type');
            $table->string('default_value')->nullable();
            $table->bigInteger('section_id')->unsigned()->nullable();
            $table->boolean('is_required')->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('section_id')->references('id')->on('section');
        });

        Schema::create('section_input_field', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('description')->nullable();
            $table->bigInteger('section_input_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('section_input_id')->references('id')->on('section_input');
        });

        Schema::create('section_mapping', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('type');
            $table->text('map');
            $table->bigInteger('section_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('section_id')->references('id')->on('section');
        });

        Schema::create('module_section', function(Blueprint $table){

            $table->bigInteger('module_id')->unsigned();
            $table->bigInteger('section_id')->unsigned();

            $table->foreign('module_id')->references('id')->on('module');
            $table->foreign('section_id')->references('id')->on('section');

        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('section_input_field');
        Schema::dropIfExists('section_input');
        Schema::dropIfExists('section_mapping');
        Schema::dropIfExists('module_section');
        Schema::dropIfExists('module_input');
        Schema::dropIfExists('module_product');
        Schema::dropIfExists('section');
        Schema::dropIfExists('module');
        Schema::dropIfExists('product');
    }
}
