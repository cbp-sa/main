<?php

use App\Models\Config\Module;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Webpatser\Uuid\Uuid;
use Carbon\Carbon;


class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('module')->insert(
            [
                'name' => 'ID Number',
                'uniqueKey' => Uuid::generate(4)->string,
                'description' => null,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()
            ]

        );

        $module  = Module::where('name', '=', 'ID Number')->first();


        DB::table('module_input')->insert(
            [
                'name' => 'IDNumber',
                'description' => 'ID number',
                'placeholder' => 'ID number',
                'type' => 'int',
                'module_id' => $module->id,
                'is_required' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()

            ]
        );

        DB::table('module_input')->insert(
            [
                'name' => 'Reference',
                'description' => 'Reference',
                'placeholder' => 'Reference',
                'type' => 'chars',
                'module_id' => $module->id,
                'is_required' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()

            ]
        );
    }
}
