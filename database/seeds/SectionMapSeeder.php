<?php

use App\Models\Config\Section;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SectionMapSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {



        $telephone  = Section::where('name', '=', 'Telephone')->first();


        DB::table('section_mapping')->insert(
            [
                'name' => 'Telephones',
                'type' => 'grid',
                'section_id' => $telephone->id,
                'map'  =>  implode(',', ['TelNumber','Links','TelType','Network','BusinessName','LatestDate','Score','FirstStatus']),
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()
            ]
        );

        $address  = Section::where('name', '=', 'Address')->first();

        DB::table('section_mapping')->insert(
            [
                'name' => 'OutputAddresses',
                'type' => 'grid',
                'section_id' => $address->id,
                'map'  =>  implode(',', ['OriginalAddress', 'LatestDate', 'Score']),
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()
            ]
        );
    }
}
