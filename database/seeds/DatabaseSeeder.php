<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    //     $this->call(ProductSeeder::class);
    //     $this->call(ModuleSeeder::class);
    //     $this->call(SectionSeed::class);
    //     $this->call(SectionMapSeeder::class);

    //    // Always run relasionship seeder last to ensure models are filled before attaching.
    //     $this->call(RelationshipSeeder::class);
    //    $this->call(CompanySeeder::class);

        $this->call(SectionSeederTypeDataLoader::class);

    }
}
