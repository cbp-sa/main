<?php

use App\Models\Config\Module;
use App\Models\Config\Product;
use App\Models\Config\Section;
use Illuminate\Database\Seeder;

class RelationshipSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product  = Product::where('name', '=', 'Directory')->first();

        $module  = Module::where('name', '=', 'ID Number')->first();

        $product->module()->attach($module->id);

        $telephone  = Section::where('name', '=', 'Telephone')->first();
        $address  = Section::where('name', '=', 'Address')->first();

        $module->sections()->attach($telephone->id);
        $module->sections()->attach($address->id);

    }
}
