<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Webpatser\Uuid\Uuid;
use Carbon\Carbon;


class CompanySeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('company')->insert(
            [
                "uniqueKey" => Uuid::generate(4)->string,
                "Title" => "Consumer Profile Bureau | Bureau of Bureaus",
                "CaptchaLimit" => 25,
                "CaptchaTime" => 120,
                "ShortName" => "CPB",
                "Name" => "Consumer Profile Bureau (Pty) Ltd",
                "Tel" => "010 590 9505",
                "Fax" => "086 556 3299",
                "Address" => "Bureau place, Turnberry Office Park, 48 Grosvenor Road, Bryanston, 2021",
                "EmailSupport" => "access@cpbonline.co.za",
                "EmailSales" => "sales@cpbonline.co.za",
                "EmailAccess" => "access@cpbonline.co.za",
                "Website" => "https://bureausuite.co.za",
                "RegistrationNumber" => "1981/007624/07",
                "NCRNumber" => "NCRCB2",
                "VATNumber" => "4320139829",
                "Directors" => "Alain Craven | James Guthrie | Lindy Mbatha. Officers: Marina Short (CEO) | Annemarie Haywood (CFO)",
                "Logo" => "logo__cpb.png",
                "DashboardImage" => "images",
                "Footprint" => "",
                "DisclaimerNotice" => "The Consumer Profile Bureau (Pty) Ltd shall not be liable for any damage or loss, whether direct or indirect, arising out of the use of or the omission to use the statement made in response to the enquiry made herein or for any consequential damages, loss of profit or special damages arising out of the issuing of that statement or any use thereof.<br>",
                "SystemType" => "CPB",
                "VendorID" => "CPB",
                "VendorPassword" => "wujament",
                "created_at" => Carbon::now()->toDateTimeString(),
                "updated_at" => Carbon::now()->toDateTimeString()
            ]);

            DB::table('company')->insert( [
                "uniqueKey" => Uuid::generate(4)->string,
                "Title" => "CrossCheck Information Bureau | Bureau of Bureaus",
                "CaptchaLimit" => 25,
                "CaptchaTime" => 120,
                "ShortName" => "CrossCheck",
                "Name" => "CrossCheck Information Bureau (Pty) Ltd",
                "Tel" => "010 590 9505",
                "Fax" => "086 556 3299",
                "Address" => "Bureau House, 1 Marie Street, Praegville, Randburg, 2196",
                "EmailSupport" => "support@crosscheckonline.co.za",
                "EmailSales" => "sales@crosscheckonline.co.za",
                "EmailAccess" => "access@crosscheckonline.co.za",
                "Website" => "https://www.crosscheckonline.co.za",
                "RegistrationNumber" => "1997/015143/07",
                "NCRNumber" => "NCRCB1",
                "VATNumber" => "4230169304",
                "Directors" => "Marina Short | Alain Craven | James Guthrie | Lindy Mbatha",
                "Logo" => "logo__cc.png",
                "DashboardImage" => "whitelabel/CrossCheck/images",
                "Footprint" => "",
                "DisclaimerNotice" => "The CrossCheck Information Bureau (Pty) Ltd shall not be liable for any damage or loss, whether direct or indirect, arising out of the use of or the omission to use the statement made in response to the enquiry made herein or for any consequential damages, loss of profit or special damages arising out of the issuing of that statement or any use thereof.<br>",
                "SystemType" => "CrossCheck",
                "VendorID" => "CPB",
                "VendorPassword" => "wujament",
                "created_at" => Carbon::now()->toDateTimeString(),
                "updated_at" => Carbon::now()->toDateTimeString()

            ]);

            DB::table('company')->insert([

                "Title" => "Medical Credit Watch",
                "uniqueKey" => Uuid::generate(4)->string,
                "CaptchaLimit" => 25,
                "CaptchaTime" => 120,
                "ShortName" => "MCW",
                "Name" => "Medical Credit Watch (Pty) Ltd",
                "Tel" => "010 590 9505",
                "Fax" => "086 556 3299",
                "Address" => "Bureau House, 1 Marie Street, Praegville, Randburg, 2196",
                "EmailSupport" => "estelle@medicalcreditwatch.co.za",
                "EmailSales" => "estelle@medicalcreditwatch.co.za",
                "EmailAccess" => "support@medicalcreditwatch.co.za",
                "Website" => "https://www.medicalcreditwatch.co.za",
                "RegistrationNumber" => "201110881007",
                "NCRNumber" => "",
                "VATNumber" => "",
                "Directors" => "Marina Short | Alain Craven",
                "Logo" => "logo__mcw.png",
                "DashboardImage" => "images",
                "Footprint" => "",
                "DisclaimerNotice" => "The Medical Credit Watch (Pty) Ltd shall not be liable for any damage or loss, whether direct or indirect, arising out of the use of or the omission to use the statement made in response to the enquiry made herein or for any consequential damages, loss of profit or special damages arising out of the issuing of that statement or any use thereof.<br>",
                "SystemType" => "MCW",
                "VendorID" => "CPB",
                "VendorPassword" => "wujament",
                "created_at" => Carbon::now()->toDateTimeString(),
                "updated_at" => Carbon::now()->toDateTimeString()
            ]
        );

    }
}
