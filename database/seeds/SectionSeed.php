<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Webpatser\Uuid\Uuid;
use App\Models\Config\Section;


class SectionSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        DB::table('section')->insert(
            [
                'name' => 'Telephone',
                'uniqueKey' => Uuid::generate(4)->string,
                'description' => 'Returns a list of telephone objects based on the given Parameters',
                'url' => 'telephone/list',
                'callingModule' => 'Directory',
                'type' => 'list',
                'is_enabled' => true,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()
            ]

        );


        DB::table('section')->insert(
            [
                'name' => 'Address',
                'uniqueKey' => Uuid::generate(4)->string,
                'description' => 'Returns a list of address objects based on the given Parameters',
                'url' => 'address/list',
                'callingModule' => 'Directory',
                'type' => 'list',
                'is_enabled' => true,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()
            ]
        );


        $section  = Section::where([['name', '=', 'Telephone'],['type','=','list']])->first();

        DB::table('section_input')->insert(
            [
                'name' => 'IDNumber',
                'description' => 'ID number',
                'placeholder' => 'ID number',
                'type' => 'int',
                'section_id' => $section->id,
                'is_required' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()

            ]
        );

        DB::table('section_input')->insert(
            [
                'name' => 'Reference',
                'description' => 'Reference',
                'placeholder' => 'Reference',
                'type' => 'chars',
                'section_id' => $section->id,
                'is_required' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()

            ]
        );


        $section  = Section::where([['name', '=', 'Address'],['type','=','list']])->first();


        DB::table('section_input')->insert(
            [
                'name' => 'IDNumber',
                'description' => 'ID number',
                'placeholder' => 'ID number',
                'type' => 'int',
                'section_id' => $section->id,
                'is_required' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()

            ]
        );

        DB::table('section_input')->insert(
            [
                'name' => 'Reference',
                'description' => 'Reference',
                'placeholder' => 'Reference',
                'type' => 'chars',
                'section_id' => $section->id,
                'is_required' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()

            ]
        );

        DB::table('section_input')->insert(
            [
                'name' => 'UseRankedScore',
                'description' => 'Use ranked score',
                'placeholder' => 'Use ranked score',
                'type' => 'bool',
                'section_id' => $section->id,
                'is_required' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()

            ]
        );

        DB::table('section_input')->insert(
            [
                'name' => 'UseMuni',
                'description' => 'Use municipal',
                'placeholder' => 'Use municipal',
                'type' => 'bool',
                'section_id' => $section->id,
                'is_required' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()

            ]
        );


        DB::table('section_input')->insert(
            [
                'name' => 'Filter',
                'description' => 'Filter',
                'placeholder' => 'Filter',
                'type' => 'filter',
                'section_id' => $section->id,
                'is_required' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()

            ]
        );


        // email segment



       DB::table('section')->insert(
        [
            'name' => 'Email',
            'uniqueKey' => Uuid::generate(4)->string,
            'description' => 'Returns a list of email objects based on the given Parameters',
            'url' => 'emailaddress/list/',
            'callingModule' => 'Directory',
            'type' => 'list',
            'is_enabled' => true,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]

    );

    $section  = Section::where([['name', '=', 'Email'],['type','=','list']])->first();


    DB::table('section_input')->insert(
        [
            'name' => 'Token',
            'description' => 'Token',
            'placeholder' => 'Token',
            'type' => 'token',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

     DB::table('section_input')->insert(
        [
            'name' => 'IDNumber',
            'description' => 'Token',
            'placeholder' => 'ID number',
            'type' => 'int',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

      DB::table('section_input')->insert(
        [
            'name' => 'Reference',
            'description' => 'Token',
            'placeholder' => 'Reference',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

    DB::table('section_input')->insert(
        [
            'name' => 'Filter',
            'description' => 'Token',
            'placeholder' => 'Filter',
            'type' => 'filter',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );






//PERSON

DB::table('section')->insert(
        [
            'name' => 'Person',
            'uniqueKey' => Uuid::generate(4)->string,
            'description' => 'Returns a random list of demographically similar names to the input person',
            'url' => 'people/',
            'callingModule' => 'Directory',
            'type' => 'list',
            'is_enabled' => true,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]

    );

    $section  = Section::where([['name', '=', 'Person'],['type','=','list']])->first();



DB::table('section_input')->insert(
        [
            'name' => 'Token',
            'description' => 'Token',
            'placeholder' => 'Token',
            'type' => 'token',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

DB::table('section_input')->insert(
        [
            'name' => 'IDNumber',
            'description' => 'Token',
            'placeholder' => 'ID number',
            'type' => 'int',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );
DB::table('section_input')->insert(
        [
            'name' => 'UseDHAExtra',
            'description' => 'Token',
            'placeholder' => 'UseDHAExtra',
            'type' => 'boolYN',
            'section_id' => $section->id,
            'is_required' => 1,
            'default_value'=>'NO',
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );
DB::table('section_input')->insert(
        [
            'name' => 'Reference',
            'description' => 'Token',
            'placeholder' => 'Reference',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

DB::table('section_input')->insert(
        [
            'name' => 'Filter',
            'description' => 'Token',
            'placeholder' => 'Filter',
            'type' => 'filter',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );



// employment segment

DB::table('section')->insert(
        [
            'name' => 'Employment',
            'uniqueKey' => Uuid::generate(4)->string,
            'description' => 'Returns a list of employer objects based on the given Parameters',
            'url' => 'emailaddres/list',
            'callingModule' => 'Directory',
            'type' => 'list',
            'is_enabled' => true,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]

    );



    $section  = Section::where([['name', '=', 'Employment'],['type','=','list']])->first();



      DB::table('section_input')->insert(
        [
            'name' => 'Token',
            'description' => 'Token',
            'placeholder' => 'Token',
            'type' => 'token',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

        DB::table('section_input')->insert(
            [
                'name' => 'IDNumber',
                'description' => 'Token',
                'placeholder' => 'IDNumber',
                'type' => 'int',
                'section_id' => $section->id,
                'is_required' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()

            ]
        );

        DB::table('section_input')->insert(
            [
                'name' => 'Reference',
                'description' => 'Token',
                'placeholder' => 'Reference',
                'type' => 'chars',
                'section_id' => $section->id,
                'is_required' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()

            ]
        );

         DB::table('section_input')->insert(
            [
                'name' => 'UseMuni',
                'description' => 'Token',
                'placeholder' => 'Use municipal',
                'type' => 'bool',
                'section_id' => $section->id,
                'is_required' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()

            ]
        );

        DB::table('section_input')->insert(
            [
                'name' => 'Filter',
                'description' => 'Token',
                'placeholder' => 'Filter',
                'type' => 'filter',
                'section_id' => $section->id,
                'is_required' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()

            ]
        );



// deceased segment


DB::table('section')->insert(
        [
            'name' => 'Deceased',
            'uniqueKey' => Uuid::generate(4)->string,
            'description' => 'Returns a list of deed objects based on the given Parameters',
            'url' => 'deceased',
            'callingModule' => 'Directory',
            'type' => 'list',
            'is_enabled' => true,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]

    );


    $section  = Section::where([['name', '=', 'Deceased'],['type','=','list']])->first();


      DB::table('section_input')->insert(
        [
            'name' => 'Token',
            'description' => 'Token',
            'placeholder' => 'Token',
            'type' => 'token',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

     DB::table('section_input')->insert(
        [
            'name' => 'IDNumber',
            'description' => 'Token',
            'placeholder' => 'IDNumber',
            'type' => 'int',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );



     DB::table('section_input')->insert(
        [
            'name' => 'UseDHAExtra',
            'description' => 'Token',
            'placeholder' => 'UseDHAExtra',
            'type' => 'boolYN',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

    DB::table('section_input')->insert(
        [
            'name' => 'Reference',
            'description' => 'Token',
            'placeholder' => 'Reference',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );


     DB::table('section_input')->insert(
        [
            'name' => 'Filter',
            'description' => 'Token',
            'placeholder' => 'Filter',
            'type' => 'filter',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );


// deeds segment



DB::table('section')->insert(
        [
            'name' => 'Deeds',
            'uniqueKey' => Uuid::generate(4)->string,
            'description' => 'Returns a list of deed objects based on the given Parameters',
            'url' => 'deeds',
            'callingModule' => 'Directory',
            'type' => 'list',
            'is_enabled' => true,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]

    );


    $section  = Section::where([['name', '=', 'Deeds'],['type','=','list']])->first();



      DB::table('section_input')->insert(
        [
            'name' => 'Token',
            'description' => 'Token',
            'placeholder' => 'Token',
            'type' => 'token',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );


    DB::table('section_input')->insert(
        [
            'name' => 'IDNumber',
            'description' => 'ID number',
            'placeholder' => 'ID number',
            'type' => 'int',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

    DB::table('section_input')->insert(
        [
            'name' => 'RegistrationNumber',
            'description' => 'Registration Number',
            'placeholder' => 'Registration Number',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

     DB::table('section_input')->insert(
        [
            'name' => 'TrustNumber',
            'description' => 'Trust Number',
            'placeholder' => 'Trust Number',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );


     DB::table('section_input')->insert(
        [
            'name' => 'Reference',
            'description' => 'Token',
            'placeholder' => 'Reference',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

    DB::table('section_input')->insert(
        [
            'name' => 'Filter',
            'description' => 'Token',
            'placeholder' => 'Filter',
            'type' => 'filter',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );



// defaults segment





DB::table('section')->insert(
        [
            'name' => 'Defaults',
            'uniqueKey' => Uuid::generate(4)->string,
            'description' => 'Returns a list of defaults objects based on the given Parameters',
            'url' => 'default/list',
            'callingModule' => 'Directory',
            'type' => 'list',
            'is_enabled' => true,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]

    );

    $section  = Section::where([['name', '=', 'Defaults'],['type','=','list']])->first();


      DB::table('section_input')->insert(
        [
            'name' => 'Token',
            'description' => 'Token',
            'placeholder' => 'Token',
            'type' => 'token',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );


   DB::table('section_input')->insert(
        [
            'name' => 'IDNumber',
            'description' => 'Token',
            'placeholder' => 'ID number',
            'type' => 'int',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

   DB::table('section_input')->insert(
        [
            'name' => 'Reference',
            'description' => 'Token',
            'placeholder' => 'Reference',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

  DB::table('section_input')->insert(
        [
            'name' => 'UseMuni',
            'description' => 'Token',
            'placeholder' => 'Use municipal',
            'type' => 'bool',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

   DB::table('section_input')->insert(
        [
            'name' => 'Filter',
            'description' => 'Token',
            'placeholder' => 'Filter',
            'type' => 'filter',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );



// debt review segment





    DB::table('section')->insert(
        [
            'name' => 'Debt Review',
            'uniqueKey' => Uuid::generate(4)->string,
            'description' => 'Returns a list of debt review objects based on the given Parameters',
            'url' => 'debtreview/list',
            'callingModule' => 'Directory',
            'type' => 'list',
            'is_enabled' => true,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]

    );


    $section  = Section::where([['name', '=', 'Debt Review'],['type','=','list']])->first();


      DB::table('section_input')->insert(
        [
            'name' => 'Token',
            'description' => 'Token',
            'placeholder' => 'Token',
            'type' => 'token',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );


   DB::table('section_input')->insert(
        [
            'name' => 'IDNumber',
            'description' => 'Token',
            'placeholder' => 'ID number',
            'type' => 'int',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

   DB::table('section_input')->insert(
        [
            'name' => 'Reference',
            'description' => 'Token',
            'placeholder' => 'Reference',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );


   DB::table('section_input')->insert(
        [
            'name' => 'Filter',
            'description' => 'Token',
            'placeholder' => 'Filter',
            'type' => 'filter',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );




// relatives segment



DB::table('section')->insert(
        [
            'name' => 'Relatives',
            'uniqueKey' => Uuid::generate(4)->string,
            'description' => 'Returns a list of relatives objects based on the given Parameters',
            'url' => 'relatives/list',
            'callingModule' => 'Directory',
            'type' => 'list',
            'is_enabled' => true,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]

    );


    $section  = Section::where([['name', '=', 'Relatives'],['type','=','list']])->first();


      DB::table('section_input')->insert(
        [
            'name' => 'Token',
            'description' => 'Token',
            'placeholder' => 'Token',
            'type' => 'token',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );


   DB::table('section_input')->insert(
        [
            'name' => 'IDNumber',
            'description' => 'Token',
            'placeholder' => 'ID number',
            'type' => 'int',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

   DB::table('section_input')->insert(
        [
            'name' => 'Reference',
            'description' => 'Token',
            'placeholder' => 'Reference',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );


   DB::table('section_input')->insert(
        [
            'name' => 'Filter',
            'description' => 'Token',
            'placeholder' => 'Filter',
            'type' => 'filter',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );


// safps segment



DB::table('section')->insert(
        [
            'name' => 'SAFPS',
            'uniqueKey' => Uuid::generate(4)->string,
            'description' => 'Returns a list of SAFPS objects based on the given Parameters',
            'url' => 'safps/list',
            'callingModule' => 'Directory',
            'type' => 'list',
            'is_enabled' => true,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]

    );


    $section  = Section::where([['name', '=', 'SAFPS'],['type','=','list']])->first();



      DB::table('section_input')->insert(
        [
            'name' => 'SAFPS',
            'description' => 'Token',
            'placeholder' => 'Token',
            'type' => 'token',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );


   DB::table('section_input')->insert(
        [
            'name' => 'IDNumber',
            'description' => 'Token',
            'placeholder' => 'ID number',
            'type' => 'int',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

   DB::table('section_input')->insert(
        [
            'name' => 'Reference',
            'description' => 'Token',
            'placeholder' => 'Reference',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );


   DB::table('section_input')->insert(
        [
            'name' => 'Filter',
            'description' => 'Token',
            'placeholder' => 'Filter',
            'type' => 'filter',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );


// safps protective segment


DB::table('section')->insert(
        [
            'name' => 'SAFPS Protective',
            'uniqueKey' => Uuid::generate(4)->string,
            'description' => 'Returns a list of SAFPS objects based on the given Parameters',
            'url' => 'safpsprotective/list',
            'callingModule' => 'Directory',
            'type' => 'list',
            'is_enabled' => true,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]

    );


    $section  = Section::where([['name', '=', 'SAFPS Protective'],['type','=','list']])->first();


      DB::table('section_input')->insert(
        [
            'name' => 'SAFPS',
            'description' => 'Token',
            'placeholder' => 'Token',
            'type' => 'token',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );


   DB::table('section_input')->insert(
        [
            'name' => 'IDNumber',
            'description' => 'Token',
            'placeholder' => 'ID number',
            'type' => 'int',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

   DB::table('section_input')->insert(
        [
            'name' => 'Reference',
            'description' => 'Token',
            'placeholder' => 'Reference',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );


   DB::table('section_input')->insert(
        [
            'name' => 'Filter',
            'description' => 'Token',
            'placeholder' => 'Filter',
            'type' => 'filter',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );



// CIPC directors segment


DB::table('section')->insert(
        [
            'name' => 'CIPC Directors',
            'uniqueKey' => Uuid::generate(4)->string,
            'description' => 'Returns a list of director objects based on the given Parameters',
            'url' => 'cipcdirectors/list',
            'callingModule' => 'Directory',
            'type' => 'list',
            'is_enabled' => true,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]

    );



    $section  = Section::where([['name', '=', 'CIPC Directors'],['type','=','list']])->first();


      DB::table('section_input')->insert(
        [
            'name' => 'Token',
            'description' => 'Token',
            'placeholder' => 'Token',
            'type' => 'token',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );


   DB::table('section_input')->insert(
        [
            'name' => 'IDNumber',
            'description' => 'Token',
            'placeholder' => 'ID number',
            'type' => 'int',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

    DB::table('section_input')->insert(
        [
            'name' => 'ExtraPersonDetails',
            'description' => 'Token',
            'placeholder' => 'ExtraPersonDetails',
            'type' => 'boolYN',
            'default_value'=>'No',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

   DB::table('section_input')->insert(
        [
            'name' => 'UseDHAExtra',
            'description' => 'Token',
            'placeholder' => 'UseDHAExtra',
            'type' => 'boolYN',
            'default_value'=>'No',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

    DB::table('section_input')->insert(
        [
            'name' => 'Reference',
            'description' => 'Token',
            'placeholder' => 'Reference',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );




// CIPC enterprises segment


DB::table('section')->insert(
        [
            'name' => 'CIPC Enterprises',
            'uniqueKey' => Uuid::generate(4)->string,
            'description' => 'Returns a list of director objects based on the given Parameters',
            'url' => 'cipcenterprises/list',
            'callingModule' => 'Directory',
            'type' => 'list',
            'is_enabled' => true,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]

    );



    $section  = Section::where([['name', '=', 'CIPC Enterprises'],['type','=','list']])->first();



      DB::table('section_input')->insert(
        [
            'name' => 'Token',
            'description' => 'Token',
            'placeholder' => 'Token',
            'type' => 'token',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );


   DB::table('section_input')->insert(
        [
            'name' => 'RegistrationNumber',
            'description' => 'Token',
            'placeholder' => 'Registration number',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

   DB::table('section_input')->insert(
        [
            'name' => 'Reference',
            'description' => 'Token',
            'placeholder' => 'Reference',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );


// CIPC auditors list


DB::table('section')->insert(
        [
            'name' => 'CIPC Auditors',
            'uniqueKey' => Uuid::generate(4)->string,
            'description' => 'Returns a list of director objects based on the given Parameters',
            'url' => 'cipcauditors/list',
            'callingModule' => 'Directory',
            'type' => 'list',
            'is_enabled' => true,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]

    );


    $section  = Section::where([['name', '=', 'CIPC Auditors'],['type','=','list']])->first();



      DB::table('section_input')->insert(
        [
            'name' => 'Token',
            'description' => 'Token',
            'placeholder' => 'Token',
            'type' => 'token',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );


   DB::table('section_input')->insert(
        [
            'name' => 'RegistrationNumber',
            'description' => 'Token',
            'placeholder' => 'Registration number',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

   DB::table('section_input')->insert(
        [
            'name' => 'Reference',
            'description' => 'Token',
            'placeholder' => 'Reference',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );






// trace locator segment


DB::table('section')->insert(
        [
            'name' => 'TraceLocator',
            'uniqueKey' => Uuid::generate(4)->string,
            'description' => 'Returns a list of trace locator objects based on the given Parameters',
            'url' => 'tracelocator/list',
            'callingModule' => 'Directory',
            'type' => 'list',
            'is_enabled' => true,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]

    );



    $section  = Section::where([['name', '=', 'TraceLocator'],['type','=','list']])->first();



      DB::table('section_input')->insert(
        [
            'name' => 'Token',
            'description' => 'Token',
            'placeholder' => 'Token',
            'type' => 'token',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );


   DB::table('section_input')->insert(
        [
            'name' => 'IDNumber',
            'description' => 'Token',
            'placeholder' => 'ID number',
            'type' => 'int',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

   DB::table('section_input')->insert(
        [
            'name' => 'Reference',
            'description' => 'Token',
            'placeholder' => 'Reference',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

     DB::table('section_input')->insert(
        [
            'name' => 'Filter',
            'description' => 'Token',
            'placeholder' => 'Filter',
            'type' => 'filter',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );


// strike dates segment

DB::table('section')->insert(
        [
            'name' => 'Strike dates',
            'uniqueKey' => Uuid::generate(4)->string,
            'description' => 'Returns a list of strike dates based on the given Parameters',
            'url' => 'strikedates/list',
            'callingModule' => 'Directory',
            'type' => 'list',
            'is_enabled' => true,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]

    );


    $section  = Section::where([['name', '=', 'Strike dates'],['type','=','list']])->first();



      DB::table('section_input')->insert(
        [
            'name' => 'Token',
            'description' => 'Token',
            'placeholder' => 'Token',
            'type' => 'token',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );


   DB::table('section_input')->insert(
        [
            'name' => 'IDNumber',
            'description' => 'Token',
            'placeholder' => 'ID number',
            'type' => 'int',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

   DB::table('section_input')->insert(
        [
            'name' => 'Reference',
            'description' => 'Token',
            'placeholder' => 'Reference',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

     DB::table('section_input')->insert(
        [
            'name' => 'Filter',
            'description' => 'Token',
            'placeholder' => 'Filter',
            'type' => 'filter',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );


// avs segment

DB::table('section')->insert(
        [
            'name' => 'AVS',
            'uniqueKey' => Uuid::generate(4)->string,
            'description' => 'Performs an AVS validation',
            'url' => 'list',
            'callingModule' => 'Directory',
            'type' => 'list',
            'is_enabled' => true,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]

   );

   $section  = Section::where([['name', '=', 'AVS'],['type','=','list']])->first();



      DB::table('section_input')->insert(
        [
            'name' => 'Token',
            'description' => 'Token',
            'placeholder' => 'Token',
            'type' => 'token',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );


   DB::table('section_input')->insert(
        [
            'name' => 'REF',
            'description' => 'Token',
            'placeholder' => 'Reference',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

   DB::table('section_input')->insert(
        [
            'name' => 'RT',
            'description' => 'Token',
            'placeholder' => 'Request Type',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

  DB::table('section_input')->insert(
        [
            'name' => 'AT',
            'description' => 'Token',
            'placeholder' => 'Account Type',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

     DB::table('section_input')->insert(
        [
            'name' => 'IT',
            'description' => 'Token',
            'placeholder' => 'Identification Type',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

    DB::table('section_input')->insert(
        [
            'name' => 'IN',
            'description' => 'Token',
            'placeholder' => 'Initials',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

   DB::table('section_input')->insert(
        [
            'name' => 'NA',
            'description' => 'Token',
            'placeholder' => 'Surname/Company Name',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

   DB::table('section_input')->insert(
        [
            'name' => 'ID',
            'description' => 'Token',
            'placeholder' => 'ID/Registration Number',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

  DB::table('section_input')->insert(
        [
            'name' => 'TX',
            'description' => 'Token',
            'placeholder' => 'Tax Number',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

  DB::table('section_input')->insert(
        [
            'name' => 'BC',
            'description' => 'Token',
            'placeholder' => 'Branch Code',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

  DB::table('section_input')->insert(
        [
            'name' => 'AN',
            'description' => 'Token',
            'placeholder' => 'Account Number',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );






   DB::table('section_input')->insert(
        [
            'name' => 'Reference',
            'description' => 'Token',
            'placeholder' => 'Reference',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

     DB::table('section_input')->insert(
        [
            'name' => 'Filter',
            'description' => 'Token',
            'placeholder' => 'Filter',
            'type' => 'filter',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );


// disputes segment

DB::table('section')->insert(
        [
            'name' => 'Disputes',
            'uniqueKey' => Uuid::generate(4)->string,
            'description' => 'Returns a list of dispute objects based on the given Parameters',
            'url' => 'dispute',
            'callingModule' => 'Directory',
            'type' => 'list',
            'is_enabled' => true,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]

    );

    $section  = Section::where([['name', '=', 'Disputes'],['type','=','list']])->first();


      DB::table('section_input')->insert(
        [
            'name' => 'Token',
            'description' => 'Token',
            'placeholder' => 'Token',
            'type' => 'token',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );


   DB::table('section_input')->insert(
        [
            'name' => 'IDNumber',
            'description' => 'Token',
            'placeholder' => 'ID number',
            'type' => 'int',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

   DB::table('section_input')->insert(
        [
            'name' => 'Reference',
            'description' => 'Token',
            'placeholder' => 'Reference',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

     DB::table('section_input')->insert(
        [
            'name' => 'Filter',
            'description' => 'Token',
            'placeholder' => 'Filter',
            'type' => 'filter',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );


// spider web relationships segment

DB::table('section')->insert(
        [
            'name' => 'Spider web relationships',
            'uniqueKey' => Uuid::generate(4)->string,
            'description' => 'Returns a list of spider web relationship objects based on the given Parameters',
            'url' => 'spiderwebrelationship',
            'callingModule' => 'Directory',
            'type' => 'list',
            'is_enabled' => true,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]

    );

    $section  = Section::where([['name', '=', 'Spider web relationships'],['type','=','list']])->first();


      DB::table('section_input')->insert(
        [
            'name' => 'Token',
            'description' => 'Token',
            'placeholder' => 'Token',
            'type' => 'token',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );


   DB::table('section_input')->insert(
        [
            'name' => 'PrimaryIDNumber',
            'description' => 'Token',
            'placeholder' => 'Primary ID number',
            'type' => 'int',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );


   DB::table('section_input')->insert(
        [
            'name' => 'SecondaryIDNumber',
            'description' => 'Token',
            'placeholder' => 'Secondary ID number',
            'type' => 'int',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

   DB::table('section_input')->insert(
        [
            'name' => 'Reference',
            'description' => 'Token',
            'placeholder' => 'Reference',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

     DB::table('section_input')->insert(
        [
            'name' => 'Filter',
            'description' => 'Token',
            'placeholder' => 'Filter',
            'type' => 'filter',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

// politicians segment

DB::table('section')->insert(
        [
            'name' => 'Politician',
            'uniqueKey' => Uuid::generate(4)->string,
            'description' => 'Returns a list of politicians that match the given national ID. Only South African politicians are supported.',
            'url' => 'politician',
            'callingModule' => 'Directory',
            'type' => 'list',
            'is_enabled' => true,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]

    );



    $section  = Section::where([['name', '=', 'Politician'],['type','=','list']])->first();


      DB::table('section_input')->insert(
        [
            'name' => 'Token',
            'description' => 'Token',
            'placeholder' => 'Token',
            'type' => 'token',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );


   DB::table('section_input')->insert(
        [
            'name' => 'IDNumber',
            'description' => 'Token',
            'placeholder' => 'ID number',
            'type' => 'int',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

   DB::table('section_input')->insert(
        [
            'name' => 'Reference',
            'description' => 'Token',
            'placeholder' => 'Reference',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

     DB::table('section_input')->insert(
        [
            'name' => 'MinMatchScore',
            'description' => 'Token',
            'placeholder' => 'Min Match Score',
            'type' => 'int',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

// IDV segment

DB::table('section')->insert(
        [
            'name' => 'IDV',
            'uniqueKey' => Uuid::generate(4)->string,
            'description' => 'Performs a IDV summary details validation (no photo included)',
            'url' => 'idv',
            'callingModule' => 'Directory',
            'type' => 'list',
            'is_enabled' => true,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]

    );


    $section  = Section::where([['name', '=', 'IDV'],['type','=','list']])->first();



      DB::table('section_input')->insert(
        [
            'name' => 'Token',
            'description' => 'Token',
            'placeholder' => 'Token',
            'type' => 'token',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );


   DB::table('section_input')->insert(
        [
            'name' => 'IDNumber',
            'description' => 'Token',
            'placeholder' => 'ID number',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );



   DB::table('section_input')->insert(
        [
            'name' => 'Reference',
            'description' => 'Token',
            'placeholder' => 'Reference',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]);



// IDV summary list

DB::table('section')->insert(
        [
            'name' => 'IDV Summary',
            'uniqueKey' => Uuid::generate(4)->string,
            'description' => 'Performs a IDV fingerprint validation',
            'url' => 'idv',
            'callingModule' => 'Directory',
            'type' => 'list',
            'is_enabled' => true,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()
        ]

    );


    $section  = Section::where([['name', '=', 'IDV Summary'],['type','=','list']])->first();



      DB::table('section_input')->insert(
        [
            'name' => 'Token',
            'description' => 'Token',
            'placeholder' => 'Token',
            'type' => 'token',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );


   DB::table('section_input')->insert(
        [
            'name' => 'IDNumber',
            'description' => 'Token',
            'placeholder' => 'ID number',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );



   DB::table('section_input')->insert(
        [
            'name' => 'FPInd1',
            'description' => 'Token',
            'placeholder' => 'Fingerprint index 1',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );

   DB::table('section_input')->insert(
        [
            'name' => 'FPImg1',
            'description' => 'Token',
            'placeholder' => 'Fingerprint 1 image',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );


DB::table('section_input')->insert(
        [
            'name' => 'FPInd2',
            'description' => 'Token',
            'placeholder' => 'Fingerprint index 2',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );
 DB::table('section_input')->insert(
        [
            'name' => 'FPImg2',
            'description' => 'Token',
            'placeholder' => 'Fingerprint 2 image',
            'type' => 'chars',
            'section_id' => $section->id,
            'is_required' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString()

        ]
    );






    }


}
