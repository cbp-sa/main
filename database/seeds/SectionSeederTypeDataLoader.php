<?php

use App\Models\Config\Section;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Webpatser\Uuid\Uuid;


class SectionSeederTypeDataLoader extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $section  = Section::where([['name', '=', 'Telephone'],['type','=','dataloader']])->first();

        DB::table('section_input')->insert(
            [
                'name' => 'IDNumber',
                'description' => 'ID number',
                'placeholder' => 'ID number',
                'type' => 'int',
                'section_id' => $section->id,
                'is_required' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()

            ]
        );

        DB::table('section_input')->insert(
            [
                'name' => 'TelType',
                'description' => 'Tel Type',
                'placeholder' => 'telType',
                'type' => 'int',
                'section_id' => $section->id,
                'is_required' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()

            ]
        );

        DB::table('section_input')->insert(
            [
                'name' => 'TelNumber',
                'description' => 'TelNumber',
                'placeholder' => 'TelNumber',
                'type' => 'int',
                'section_id' => $section->id,
                'is_required' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()

            ]
        );

        DB::table('section_input')->insert(
            [
                'name' => 'Status',
                'description' => 'Status',
                'placeholder' => 'Status',
                'type' => 'hidden',
                'default_value' => 'Confirmed Correct',
                'section_id' => $section->id,
                'is_required' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString()

            ]
        );

    }
}
