<?php

/*
|--------------------------------------------------------------------------
| Admin Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin web routes for your application. These
| routes are loaded by the RouteServiceProvider.
|
*/


Route::get('/dashboard', 'AdminController@index')->name('admin-index');


// crud routes
Route::get('/crud/get/{route}/{model}/{parent}',  'Entities\CrudController@children');
Route::get('/crud/attach/{route}/{model}/{parent}/{child}',  'Entities\CrudController@attach');


// routes for managing products

Route::get('/product/index','Entities\CrudController@index')->name('admin-product-index');
Route::post('/product/create','Entities\CrudController@create')->name('admin-product-edit');
Route::get('/product/edit/{uniqueKey}','Entities\CrudController@edit')->name('admin-product-edit');
Route::post('/product/update/{uniqueKey}','Entities\CrudController@update')->name('admin-product-update');

Route::get('/product/delete/{uniqueKey}',  'Entities\CrudController@delete')->name('admin-product-delete');

// product section routes

Route::get('/section/index','Entities\CrudController@index')->name('admin-product-section-index');
Route::post('/section/create','Entities\CrudController@create')->name('admin-product-edit');
Route::get('/section/edit/{uniqueKey}','Entities\CrudController@edit')->name('admin-section-edit');
Route::post('/section/update/{uniqueKey}','Entities\CrudController@update')->name('admin-product-update');


// product section routes

Route::get('/module/index','Entities\CrudController@index')->name('admin-module-index');
Route::post('/module/create','Entities\CrudController@create')->name('admin-module-edit');
Route::get('/module/edit/{uniqueKey}','Entities\CrudController@edit')->name('admin-module-edit');
Route::post('/module/update/{uniqueKey}','Entities\CrudController@update')->name('admin-module-update');

