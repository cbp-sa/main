<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\Entities\BureauSuite\DataLoaderController;
use App\Http\Controllers\HomeController;

Auth::routes();

Route::get('/', 'HomeController@welcome');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/test', 'TestController@index')->name('test');



Route::get('sys/token/test/{token?}', 'TokenController@test');

Route::get('sys/token/new', 'TokenController@new');

Route::get('sys/token/hardcode','TokenController@hardcode');

// auth controllers

Route::post('/auth', 'AuthController@login');

// helpers


Route::get('/navigation/{type}', 'NavigationController@get');

Route::get('/make/module/pdf/{product}/{module}/{uuid}/', 'Tools\PdfController@make_module');

Route::get('/show/header', 'HelperHttpController@showHeader');

Route::get('/switch/layout/{layout}', 'HelperHttpController@switchLayout')->name('switch-layout');

Route::get('/switch/company/{company}', 'HelperHttpController@switchCompany')->name('switch-company');

Route::post('/dataloader/validation/{uniqueKey}', 'Entities\BureauSuite\DataLoaderController@validation');


// system routes

Route::get('/product/{group?}', 'Entities\BureauSuite\ProductController@index')->name('sections');

Route::get('/product/get/{uniqueKey}', 'Entities\BureauSuite\ProductController@get');

Route::post('/sections/data/{uniqueKey}', 'Entities\BureauSuite\DataController@get')->name('section-data');

Route::get('/sections/favourite/{id}', 'Entities\BureauSuite\FavouriteController@increment')->name('section-fav');



