<?php

namespace App\Exceptions;

use Exception;

class APIHTTPException extends Exception
{

    public function __construct($message, $code){

        parent::__construct();
        $this->message = $message;
        $this->code = $code;
    }


    public function report()
    {
        //
    }

    public function render($request){



        if($request->expectsJson()) {
            return $this->ajax();
        }
        return redirect()->back()->withErrors($this->getMessage())->withInput();

    }


    public function ajax(){

        return response()->json(['message'=> $this->message->getMessage(),
        'errors'=>[]], intval($this->code));
    }


    public function withData(array $data)
    {
        $this->data = $data;

        return $this;
    }

    public function withStatus($status)
    {
        $this->status = $status;

        return $this;
    }

}
