<?php

namespace App\Http\Controllers\Entities\BureauSuite;

use App\Http\Controllers\Controller;
use App\Models\Config\Favourate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FavouriteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function increment($id){

        $id = filter_var(trim(strip_tags($id)), FILTER_VALIDATE_INT);

        if(Favourate::where('module_id', $id)->exists()){
            Favourate::where('module_id', $id)->first()->increment('module_use_count');
        }else{
            $fav = new Favourate();
            $fav->module_id = $id;
            $fav->userCode = Auth::user()->userCode;
            $fav->module_use_count = 1;
            $fav->save();
        }

    }
}
