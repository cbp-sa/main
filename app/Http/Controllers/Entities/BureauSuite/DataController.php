<?php

namespace App\Http\Controllers\Entities\BureauSuite;

use App\Http\Controllers\Controller;
use \App\Helpers\BureauHouse\Api\FetchData;
use App\Helpers\BureauHouse\Tools\DataMap;
use App\Models\Config\Section;
use Illuminate\Http\Request;
use App\Helpers\Traits\CacheHelper;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use stdClass;

class DataController extends Controller
{
    use CacheHelper;

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function get(Request $request, $uniqueKey){

        $section = Section::where('uniqueKey', $uniqueKey)->first();
        $webservice = new FetchData();
        $result = $webservice->get($section, $request->all(), true);
        $dataLoader = $this->getDataLoaderModal($section);

        $this->cacheResult($request,$result, $uniqueKey);

        return response()->json(['result' =>$result, 'section' =>$section,'dataLoader' => $dataLoader]);

    }

    public function makeSectionDataKey(Request $request, $uniqueKey){
        // uuid is the unique idetifier for the submit request
        // module is the parent for what section is being called
        // uniquekey is the section uniquekey

        return $request->input('uuid') ."-". $request->input('module') . "-". $uniqueKey;
    }

    public function storeRequest($uuid, $request){

        $this->storeCache($uuid.'-request', $request);

    }

    public function cacheResult($request, $result, $uniqueKey){
        $this->storeRequest($request->input('uuid'),$request->all());
        $key = $this->makeSectionDataKey($request, $uniqueKey);

        $this->storeCache($key, $result);
    }

    public function getDataLoaderModal($section){

        if($section->can_add){
            $dataLoader =  Section::with(['input'=>function($query){
                return $query->with('field');
            }])->where([
                ['name', '=',$section->name],
                ['type', '=','dataloader']
            ])->first();

            return $this->buildModalConfig($dataLoader);
        }else{
            return null;
        }


    }


    public function buildModalConfig(Section $section){


        $modalConfig = new stdClass();
        $button =  new stdClass();
        $submitUrl = new stdClass();
        $attributes = $this->inputToAttributes($section->input);
        $submitUrl->url ="/dataloader/validation/";
        $submitUrl->id =  $section->uniqueKey;


        $button->buttonTitle ="Submit";
        $button->buttonClass= 'btn btn-info float-right';
        $button->submitUrl = $submitUrl;
        $modalConfig->modalTitle ="Add New ". $section->name . " Data";
        $modalConfig->buttons = [$button];
        $modalConfig->attributes = $attributes;
        $modalConfig->data =  $this->setInputDefaultValues($section->input);

        return $modalConfig;
    }

    public function inputToAttributes($inputs){

        $output = [];
        $inputs->each(function($value)use(&$output){
            $new =  new stdClass();


            $new->name = $value->name;
            $new->displayName = $value->placeholder;
            $new->type = $value->type;
            if($value->type=='select'){
                $keys = explode(',',$value->default_value);
                $new->options = $value->field->map(function($value)use($keys){
                    $new = new stdClass();
                    $new->value = $value[$keys[0]];
                    $new->text = $value[$keys[1]];
                    return $new;
                });
            }


            array_push($output, $new);
        });

        return $output;

    }

    public function setInputDefaultValues($inputs){

        $dataObj =  new stdClass();
        $inputs->each(function($value)use(&$dataObj){
            if($value->type!=='select' && $value->default_value){
                $dataObj->{$value->name} = $value->default_value;
            }

        });

        return $dataObj;


    }




}
