<?php

namespace App\Http\Controllers\Entities\BureauSuite;


use App\Http\Controllers\Controller;
use App\Models\Config\Module;
use App\Models\Config\Product;
use App\Models\Config\Section;
use Illuminate\Http\Request;

class SectionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($group = null){

        if(is_null($group)){
            return view('sections.index');
        }


        $module =  $this->getProduct($group);

        return view('sections.index')->with([
            'config' => $module
        ]);

    }

    public function get($product){

        $product = $this->getProduct($product);

        return response()->json($product);

    }

    public function getProduct($product){

        $product = Product::with(['module' =>function($query){
            return $query->with(['section','input']);
        }])->where('name', $product)->first();

        return $product;

    }

    public function getFullModel($product){

        $product = Product::with(['module' =>function($query){
            return $query->with(['section' => function($query){
                return $query->with('input');
            }, 'input']);
        }])->where('name', $product)->get();


    }

}
