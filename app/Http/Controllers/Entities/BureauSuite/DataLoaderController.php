<?php

namespace App\Http\Controllers\Entities\BureauSuite;

use App\Helpers\BureauHouse\Api\LoadData;
use App\Http\Controllers\Controller;
use App\Models\Config\Section;
use Exception;
use Illuminate\Http\Request;
use stdClass;

class DataLoaderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function validation(Request $request, $uniqueKey){


        $section = Section::where('uniqueKey', $uniqueKey)->first();

        if(!$section){
            throw new Exception("Invalid Section Provided");
        }


        $this->validate($request, $this->validationRulesFromInput($section->input));

        $webservice = new LoadData();
        $result = $webservice->load($section, $request->all());

        return response()->json($result);

    }

    private function validationRulesFromInput($inputs){

        $output =  [];
        $inputs->only('name', 'rules', 'is_required')->map(function($value)use(&$output){
            if($value->is_required){ $rules[] = 'required';}
            $rules[] = $value->rules;
            $output[$value->name] = implode("|", $rules);
        });

        return $output;

    }





}
