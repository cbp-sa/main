<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use stdClass;

class TestController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('test');
    }

    public function getGridConfig(){

    }

    public function getConfig(){

        return json_decode("{
            title: 'User Analysis Daily Summary',
            searchQuery: '',
            filter: true,
            groupBy: [],
            gridColumns: [
                { name: 'test_1', displayName:'Test', align:'text-left' , type:'text', filter:true, filterType:'string', filterValue:'' },
                { name: 'test_2', displayName:'Test', align:'text-left', type:'text'},
                { name: 'test_3', displayName:'Test', align:'text-left', type:'text'},
                {
            ]
            }");
    }

    public function getData(){

        return json_decode("[
            { test_1: 'value1', test_2:'value2', test_3:'value3'},
        ]");
    }
}
