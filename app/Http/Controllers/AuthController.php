<?php

namespace App\Http\Controllers;

use App\Helpers\BureauHouse\Api\Components\Auth;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function __construct(){
         

    }

    public function login(Request  $request){

        $this->validate($request, [
            'accountNumber' => 'required',
            'userCode' => 'required',
            'password' => 'required'
        ]);

        $accountNumber = $request->input('accountNumber');
        $userCode = $request->input('userCode');
        $password = $request->input('password');

        $api =  new Auth($accountNumber, $userCode, $password);



        
    }


}
