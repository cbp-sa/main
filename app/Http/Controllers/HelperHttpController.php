<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;



class HelperHttpController extends Controller
{

    public function __construct()
    {
         $this->middleware('auth');
    }

    public function showHeader(){


        if(Str::contains(config('template.layout'), 'vertical')){
            return response()->json(true);
        }

        return response()->json(false);
    }

    public function switchLayout($layout){

        Cache::store('file')->put('layout', $layout);

        return back();
    }

    public function switchCompany($company){
        Cache::store('file')->put('company', $company);

        return back();
    }

}
