<?php

namespace App\Http\Controllers\Admin\Entities;

use App\Http\Controllers\Controller;
use Request;

class CrudController extends Controller
{

    public $title;
    public $route;


    public function __construct()
    {
        $this->middleware('auth');

        $segment =  Request::segment(2);
        $this->title =   $segment .' Management';
        $this->route =  $segment;
    }

    public function index(){

        $model = "App\Models\Config\\" .  ucfirst($this->route);
        $data = $model::paginate(20);
        $fieldsConfig = (new $model)->getFieldsConfig();

        return view('admin/entities/crud/index')->with(
            [
                'data' => $data,
                'title' => $this->title,
                'route' => $this->route,
                'fields' => $fieldsConfig
            ]
        );
    }

    public function edit($uniqeKey){

        $model = "App\Models\Config\\" .  ucfirst($this->route);
        $product = $model::where('uniqueKey', $uniqeKey)->first();
        $fieldsConfig = $product->getFieldsConfig();


        return view('admin/entities/crud/edit')->with(
            [
                'data' => $product,
                'title' => $this->title,
                'route' => $this->route,
            ]
        );

    }

    public function children($route,$model, $parent){


        $instance = "App\Models\Config\\" .  ucfirst($route);
        $self = $instance::where('uniqueKey', $parent)->first();

        $relationship_model =  "App\Models\Config\\" . ucfirst($model);
        $modelData = $relationship_model::get();


        $output = [
            'parent' => $self,
            'children' =>  $self->{$model},
            'data' => $modelData
        ];

        return response()->json($output);


    }

    public function attach($route,$model, $parent, $child){

        $instance = "App\Models\Config\\" .  ucfirst($route);
        $self = $instance::where('uniqueKey', $parent)->first();

        if($self->{$model}()->attach($child)){

        }

        if($self->{$model}->contains($child)){
            $relationship_model =  "App\Models\Config\\" . ucfirst($model);
            return $relationship_model::find($child);
        }


        abort(403, 'An Error has occured');

    }

    public function create(Request $request){


        $instance = "App\Models\Config\\" .  ucfirst($request::segment(2));

        $formData = $request::all();
        return $instance::create($formData);



    }

    public function update(Request $request, $uniqueKey){

        $instance = "App\Models\Config\\" .  ucfirst($request::segment(2));

        $formData = $request::all();
        return $instance::where('uniqueKey', $uniqueKey)->update($formData);

    }

    public function delete(Request $request, $uniqueKey){

        $instance = "App\Models\Config\\" .  ucfirst($request::segment(2));

        return $instance::where('uniqueKey', $uniqueKey)->delete();
    }

}
