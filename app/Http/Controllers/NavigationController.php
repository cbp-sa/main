<?php

namespace App\Http\Controllers;

use App\Models\Config\Product;
use App\Models\Layout\Navbar;
use Illuminate\Http\Request;

class NavigationController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function get($type){

        $data = [];

        switch ($type) {
            case 'side':
                $data =  $this->side();
                break;
            case 'products':
                $data =  $this->products();
                break;

            default:

                break;
        }

        return $data;
    }

    public function side(){

        // todo add side menu logic, probably filtered by role and company.

        $navItems  = Navbar::where(['type' => function($query){
           return  $query->where('name', 'side');
        }])->get();

        return response()->json($navItems);


    }

    public function products(){

        $navItems = Product::all();

        return response()->json($navItems);
    }
}
