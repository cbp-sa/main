<?php

namespace App\Http\Controllers;

use App\Helpers\BureauHouse\Api\Auth as ApiAuth;
use App\Helpers\BureauHouse\Api\Validate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class TokenController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
         $this->middleware('auth');
    }


    public function test($token = null){

        $new = new Validate($token);

        return response()->json([$new->validate()]);
    }

    public function new(){

        $new = new Validate();
        $new->init(true);
        return response()->json([$new->validate(),Auth::user()->token]);
    }

    public function hardcode(){

        $auth = new ApiAuth('300004','T_3000040002','D9n6GamEP');
        return $auth->login(true);
    }

}
