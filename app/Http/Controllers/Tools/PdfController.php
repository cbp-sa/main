<?php

namespace App\Http\Controllers\Tools;

use App\Http\Controllers\Controller;
use App\Helpers\WKHTMLPDF\Pdf;
use App\Models\Config\Section;
use Illuminate\Http\Request;
use App\Helpers\Traits\CacheHelper;
use App\Models\Config\Product;
use stdClass;
use Illuminate\Database\Eloquent\Model;

class PdfController extends Controller
{
    use CacheHelper;
    public function __construct(){

    }

    public function make(){

        $pdf = new Pdf(true);
        $pdf->addPage(view('templates.pdf.a4')->render());
        $pdf->addPage(view('templates.pdf.a4')->render());
        return  $pdf->send();
    }


    public function make_module($product, $module, $uuid){

        $model = Product::where('uniqueKey', $product)->with(
            [
                'module' => function($query)use($module){
                    $query->where('uniqueKey', $module)->with('section');
                }
            ]
        )->first();

        // get section data from cache map to model

        $model->module->first()->section->map(function($section)use($module, $uuid){

            $section->data = $this->getSectionData($module, $section->uniqueKey, $uuid);

        });

        // get request that was sent from cache;

        $requestParameters = $this->getFromCache($uuid.'-request');



      //  return response()->view('templates.pdf.reports.product', [ 'model' => $model]);

        $pdf = new Pdf(true);

        $pdf->addPage(view('templates.pdf.reports.product', [ 'model' => $model])->render());

        return  $pdf->send();

    }


    private function getSectionData($module, $section, $uuid){

        $output = collect();

        $key =  $uuid ."-". $module. "-". $section;

        return $this->getFromCache($key);
    }

}
