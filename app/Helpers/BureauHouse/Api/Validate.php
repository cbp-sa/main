<?php

namespace App\Helpers\BureauHouse\Api;

use App\Helpers\BureauHouse\Api\WebService;
use Carbon\Carbon;
use stdClass;
use Illuminate\Support\Facades\Auth;

class Validate extends WebService{

    public function __construct(){
        parent::__construct();
    }

    public function validate($token = null){

        if(is_null($token)){
            $token = Auth::user()->token;
        }

        $result = $this->check($token);

        return  [
            'valid' => $result->getBody()->getContents() == 'OK',
            'until' => Auth::user()->token_timeout,
            'result' => [
                'code' => $result->getStatusCode(),
                'content' => $result->getBody()->getContents()
            ]
        ];
    }

    public function truthy($token){

        $result = $this->check($token);

        return $result->getBody()->getContents() == 'OK';
    }

    public function check($token){
        $result = $this->connection->request('GET',config('bureausuite.components.validate.uri'), [
            'query' => [
                'Token' => $token,
                "CallingModule" => config('bureausuite.components.validate.module')
            ]
        ]);

        return $result;
    }
}
