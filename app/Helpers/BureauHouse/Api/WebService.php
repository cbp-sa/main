<?php

namespace App\Helpers\BureauHouse\Api;

use App\Helpers\BureauHouse\Api\Auth as ComponentsAuth;
use Carbon\Carbon;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use stdClass;

abstract class WebService{



    public $connection;
    private $token;


     public function __construct(){

        $this->connection = new Client(['base_uri' => config('bureausuite.api_uri'), 'verify' => false]);

     }


     public function init($forceNewToken = false){
         // init can only be used once loged in, Auth guard must have a valid user.


        $token = null;

        if(Auth::guest()){

            throw new Exception('Error, please log in.');

        }else{

            $token_timeout =  Carbon::parse(Auth::user()->token_timeout);

            if($token_timeout->lessThan(Carbon::now()) || $forceNewToken ){

                $login = new ComponentsAuth(Auth::user()->accountNumber,  Auth::user()->userCode, Auth::user()->password);
                $result = $login->login();

                $this->token = $result->token;
                $this->setToken($result->token, $result->timestamp);
            }else{

                $this->token = Auth::user()->token;

            }
        }

    }

    private function setToken($token, $timestamp){

        $identifier = base64_encode(Auth::user()->accountNumber);
        $sessionData = decrypt(session($identifier));

        $saveData = encrypt(array_merge($sessionData,['token' => $token,'token_timeout' => $timestamp]));
        Session::put($identifier,$saveData);


    }


    public function getToken(){

        return $this->token;
    }


}


