<?php

namespace App\Helpers\BureauHouse\Api;


use App\Helpers\BureauHouse\Api\WebService;
use App\Helpers\BureauHouse\Tools\DataMap;
use App\Models\Config\Mapping;
use App\Models\Config\Section;
use Exception;
use Illuminate\Support\Facades\Auth;


class LoadData extends WebService {

    public function __construct()
    {

        parent::__construct();
        $this->init();

    }

    public function load(Section $section,  array $parameters){

        $result = $this->connection->request('GET', $section->url, [
            'query' => array_merge([
                'Token' => $this->getToken(),
            ], $parameters)
        ]);


        if($result->getStatusCode() == 200){

            $tmp = $result->getBody()->getContents();
            return collect(json_decode($tmp));
        }

         throw new Exception("Request was not successful");
    }

}
