<?php

namespace App\Helpers\BureauHouse\Api;

use App\Exceptions\APIHTTPException;
use App\Helpers\BureauHouse\Api\WebService;
use App\Helpers\BureauHouse\Tools\DataMap;
use App\Models\Config\Mapping;
use App\Models\Config\Section;
use Exception;
use Illuminate\Support\Facades\Auth;


class FetchData extends WebService {



    public function __construct()
    {

        parent::__construct();

        $this->init();

    }

    public function get(Section $section, array $parameters, $map = false){

        try {
            $response = $this->connection->request('POST', $section->url, [
                'query' => array_merge([
                    'Token' => $this->getToken(),
                ], $parameters)
            ]);

            $tmp = $response->getBody()->getContents();

            if($map == true){
                return [
                    'title' => $section->name,
                    'data' => $this->mapGrid($tmp)
                ];

            }

            return collect(json_decode($tmp));

        } catch (\Exception  $ex) {
            dd($ex);
            throw new APIHTTPException($ex->getResponse()->getBody()->getContents(), $ex->getResponse()->getStatusCode());
        }

    }

    public function mapGrid($data){

        $outputDataMap = [];

        collect(json_decode($data))->filter(function($value)use(&$outputDataMap){
            return is_array($value) && count($value) > 0;
        })->each(function($item,$key)use(&$outputDataMap){


            $mapping = Mapping::select('map')->where('name', $key)->where('type', 'grid')->first();

            if(is_null($mapping)){
                $outputDataMap[$key] = collect($item);
            }else{
                $outputDataMap[$key] = collect($item)->transform(function($value)use($mapping){
                    return collect($value)->only(explode(',', $mapping->map))->all();
                });

            }

        });

        return $outputDataMap;

    }

}
