<?php

namespace App\Helpers\BureauHouse\Api;

use App\Helpers\BureauHouse\Api\WebService;
use Carbon\Carbon;
use stdClass;


class Auth extends WebService{

    public $accountNo;
    public $userCode;
    public $password;

    public function __construct($accountNo, $userCode, $password)
    {
        parent::__construct();
        $this->accountNo= $accountNo;
        $this->userCode = $userCode;
        $this->password = $password;
    }

    public function login($raw =false){

        $result = $this->connection->request('GET',config('bureausuite.components.token.uri'), [
            'query' => [
                'AccountNumber' => $this->accountNo,
                'UserCode' => $this->userCode,
                'Password' => $this->password,
                'BureauName' => config('bureausuite.bureau_name'),
                'VendorName' => config('bureausuite.vendor'),
                "CallingModule" => config('bureausuite.componseents.token.module')
            ]
        ]);

        $tmp =  new stdClass();
        $tmp->token = $result->getBody()->getContents();
        $tmp->timestamp = Carbon::now()->addMinutes(10)->toDateTimeString();
        $tmp->login_status = false;

        if($raw){
            return $result->getBody()->getContents();
        }

        //reject if api does not respond with ok;
        if($result->getStatusCode() == 200 && $this->validate($tmp->token)){
            $tmp->login_status = true;
        }

        return  $tmp;

    }

    public function validate($token){
        $new = new Validate();
        return $new->truthy($token);
    }






}
