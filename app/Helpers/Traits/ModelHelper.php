<?php

namespace App\Helpers\Traits;

use ErrorException;
use Illuminate\Support\Facades\Schema;
use stdClass;

trait ModelHelper
{

    public function getFields(){

        $output = collect();

        foreach ($this->fillable as $key => $value) {
            $tmp = new stdClass();
            $tmp->name = $value;
            $tmp->type= Schema::getColumnType($this->table, $value);
            $output->push($tmp);
        }

        return $output;

    }

    public function getEmptyObject(){
        $new =  new stdClass();
        foreach($this->fillable as $item){
            $new->{$item} = null;
        }

        return collect($new);
    }

    public function getFieldsConfig(){

        $fields = $this->getFields();
        $fields->map(function($value){

            switch ($value->type) {
                case 'bigint':
                    if(strpos($value->name, '_id') !== false){
                        $modelNames = ucfirst(str_replace('_id','',$value->name));
                        $relationship_model =  "App\Models\Config\\"   .$modelNames;
                        $value->data = $relationship_model::select('id','name')->get()->toArray();
                    }else{
                        $value->data = null;
                    }
                break;
                case 'boolean':
                    $value->data = 0;
                break;
                default:
                    $value->data = null;
                break;
            }

            return $value;

        });

        return $fields;
    }


}
