<?php
namespace App\Helpers\Traits;

use ErrorException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;

trait CacheHelper {

    public function storeCache($key ,$result){

        Cache::store('file')->put($key,$result);

    }

    public function getFromCache($key){

        return Cache::store('file')->get($key);
    }



}
