<?php

namespace App\Helpers\WKHTMLPDF;


class File {


    // flag for deleting temp file when no longer referenced.
    public $delete =  true;
    protected $_fileName;

    public function __construct($content, $suffix = null, $prefix = null, $directory = null){
        if ($directory === null) {
            $directory = self::getTempDir();
        }

        if ($prefix === null) {
            $prefix = 'php_tmpfile_';
        }

        $this->_fileName = tempnam($directory, $prefix);
        if($suffix !== null){
            $newName = $this->_fileName.$suffix;
            rename($this->_fileName, $newName);
            $this->_fileName = $newName;
        }

        file_put_contents($this->_fileName, $content);

    }


    public function __destruct()
    {
        if($this->delete){
            unlink($this->_fileName);
        }
    }

    // send tmp file to inline as download.
    public function send($filename = null, $contentType, $inline = false){

        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Content-Type: '.$contentType);
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: '.filesize($this->_fileName));

        if ($filename!==null || $inline) {
            $disposition = $inline ? 'inline' : 'attachment';
            header("Content-Disposition: $disposition; filename=\"$filename\"");
        }
        readfile($this->_fileName);

    }

    public function saveAs($name){
        return copy($this->_fileName, $name);
    }

    public function getFileName()
    {
        return $this->_fileName;
    }

    public static function getTempDir()
    {
        if (function_exists('sys_get_temp_dir')) {
            return sys_get_temp_dir();
        } elseif ( ($tmp = getenv('TMP')) || ($tmp = getenv('TEMP')) || ($tmp = getenv('TMPDIR')) ) {
            return realpath($tmp);
        } else {
            return '/tmp';
        }
    }

    public function __toString()
    {
        return $this->_fileName;
    }


}
