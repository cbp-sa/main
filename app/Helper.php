<?php

if (!function_exists('classActivePath')) {
    function classActivePath($segment, $value)
    {
        if(!is_array($value)) {
            return Request::segment($segment) == $value ? ' menu-open' : '';
        }
        foreach ($value as $v) {
            if(Request::segment($segment) == $v) return ' menu-open';
        }
        return '';
    }
}

if (!function_exists('classActiveSegment')) {
    function classActiveSegment($segment, $value)
    {
        if(!is_array($value)) {
            return Request::segment($segment) == $value ? 'active' : '';
        }
        foreach ($value as $v) {
            if(Request::segment($segment) == $v) return 'active';
        }
        return '';
    }
}

if (!function_exists('CrudHelperBaseUrl')) {
    function CrudHelperBaseUrl()
    {
       return '/'. Request::segment(1) .'/'. Request::segment(2) .'/';
    }
}


if (!function_exists('requerst_uuid')) {
    function requerst_uuid()
    {
       return Webpatser\Uuid\Uuid::generate(4)->string;
    }
}
