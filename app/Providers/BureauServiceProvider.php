<?php
namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class BureauServiceProvider extends ServiceProvider
{
    protected $policies = [
        
    ];

    public function boot()
    {
        $this->registerPolicies();

        Auth::provider('bureau', function ($app, array $config) {
            return new \App\Providers\ApiUserProvider($this->app['hash']);
        });
    }
}