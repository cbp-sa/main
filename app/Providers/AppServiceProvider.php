<?php

namespace App\Providers;

use App\Company;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        $this->setupTemplate();
        $this->setupBladeComponents();

    }

    private function setupTemplate(){

        $company = (empty(Cache::store('file')->get('company'))) ? config('app.company.shortname') : Cache::store('file')->get('company');
        $layout = (empty(Cache::store('file')->get('layout'))) ? env('APP_LAYOUT', 'horizontal') : Cache::store('file')->get('layout');

        config([
            'company' => array_change_key_case(Company::where('ShortName', strtoupper($company))->first()->toArray(), CASE_LOWER)
        ]);

        config([
            'template' => array_change_key_case(['layout' =>'layouts.'.strtolower($layout).'.master'], CASE_LOWER)
        ]);
    }

    private function setupBladeComponents(){

        // general use components for front end.

        Blade::component('components.sections._masthead', 'masthead');
        Blade::component('components.sections._calltoaction', 'calltoaction');
        Blade::component('components.sections._features', 'features');
        Blade::component('components.sections._showcase', 'showcase');
        Blade::component('components.sections._testimonials', 'testimonials');


        // Admin crud partials
        Blade::component('components.crud._table', 'crud_table');
        Blade::component('components.crud._form', 'crud_form');


        Blade::component('components.crud._belongstomany', 'crud_belongstomany');
        Blade::component('components.crud._hasmany', 'crud_hasmany');

        // pdf template components

        Blade::component('templates.pdf.components.heading', 'header');
        Blade::component('templates.pdf.components.pagebreak', 'pagebreak');
        Blade::component('templates.pdf.components.table', 'table');
        Blade::component('templates.pdf.components.paragraph', 'paragraph');
    }


}
