<?php

namespace App\Providers;

use App\Helpers\BureauHouse\Api\Auth as ComponentsAuth;
use App\Helpers\BureauHouse\Api\Validate;
use App\User as AppUser;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;
use Illuminate\Support\Facades\Session;


class ApiUserProvider implements UserProvider
{
    protected $hasher;

    public function __construct(HasherContract $hasher)
    {

        $this->hasher = $hasher;
    }

    public function retrieveByCredentials(array $credentials)
    {

        $user = [];

        $api = new ComponentsAuth($credentials['accountNumber'],$credentials['userCode'],$credentials['password']);

        $result = $api->login();

        if($result->login_status){

            $response = new AppUser();

            $response->accountNumber = $credentials['accountNumber'];
            $response->userCode = $credentials['userCode'];
            $response->password = $credentials['password'];
            $response->id = $credentials['accountNumber'];
            $response->token =  $result->token;
            $response->token_timeout = $result->timestamp;
            $response->remember_token =  null;

            $user = $response;

            $saveData = encrypt([
                'accountNumber' => $response->accountNumber,
                'userCode' => $response->userCode,
                'password' => $response->password,
                'id' => $response->id,
                'token' => $response->token,
                'token_timeout' => $response->token_timeout,
                'remember_token' => $response->remember_token
            ]);


            Session::put(base64_encode($response->id),$saveData);

        }

        $user = $user ? : null;

        return $this->getApiUser($user);
    }

    public function retrieveById($identifier)
    {
        $user = null;
        $identifier = base64_encode($identifier);

        if(session()->has($identifier)){

            error_log('-----YES------');
            $sessionData = decrypt(session($identifier));

            $response = new AppUser();
            $response->accountNumber =  $sessionData['accountNumber'];
            $response->userCode =  $sessionData['userCode'];
            $response->password =  $sessionData['password'];
            $response->id =  $sessionData['id'];
            $response->token =  $sessionData['token'];
            $response->token_timeout = $sessionData['token_timeout'];
            $response->remember_token =  $sessionData['remember_token'];

            $user = $response;
        }

        return $this->getApiUser($user);
    }

    public function validateCredentials(UserContract $user, array $credentials)
    {

        return ($credentials['password'] === $user->getAuthPassword());
    }

    protected function getApiUser($user)
    {
        if ($user !== null) {
            return $user;
        }
    }

    protected function getUserById($id)
    {
        $user = session()->get(base64_encode($id));
        return $user ?: null;
    }

    public function retrieveByToken($identifier, $token) { }
    public function updateRememberToken(UserContract $user, $token) { }
}
