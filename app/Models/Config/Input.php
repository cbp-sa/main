<?php

namespace App\Models\Config;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Input extends Model
{
    use SoftDeletes;

    protected $table = 'section_input';

    public function field(){

        return $this->hasMany('App\Models\Config\Field','section_input_id');

    }



}
