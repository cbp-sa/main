<?php

namespace App\Models\Config;

use App\Helpers\Traits\ModelHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ModuleInput extends Model
{
    use SoftDeletes;
    use ModelHelper;

    protected $table = 'module_input';

    protected $fillable = ['name','description','module_id'];

    public function module(){
        return $this->belongsTo('App\Models\Config\Module');
    }



}
