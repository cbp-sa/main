<?php
namespace App\Models\Config;

use App\Helpers\Traits\ModelHelper;
use App\Helpers\Traits\RelationshipsHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Module extends Model
{
    use SoftDeletes;
    use RelationshipsHelper;
    use ModelHelper;

    protected $table = 'module';

    protected $fillable = ['name', 'description'];

    public function product(){

        return $this->belongsToMany('App\Models\Config\Product', 'module_product', 'product_id', 'module_id')->using('App\Models\Pivots\ModuleProduct');

    }

    public function input(){
        return $this->hasMany('App\Models\Config\ModuleInput');
    }

    public function section(){

        return $this->belongsToMany('App\Models\Config\Section', 'module_section', 'module_id', 'section_id')->using('App\Models\Pivots\ModuleSection');

    }

    public function favourate(){

        return $this->hasOne('App\Models\Config\Favourate');
    }


}
