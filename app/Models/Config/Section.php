<?php
namespace App\Models\Config;

use App\Helpers\Traits\ModelHelper;
use App\Helpers\Traits\RelationshipsHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Section extends Model
{
    use SoftDeletes;
    use RelationshipsHelper;
    use ModelHelper;

    protected $table = 'section';

    protected $fillable = ['name','uniqueKey', 'description', 'url','callingModule','is_enabled'];

    public function input(){
        return $this->hasMany('App\Models\Config\Input');
    }

    public function module(){

        return $this->belongsToMany('App\Models\Config\Module', 'module_section', 'module_id', 'section_id')->using('App\Models\Pivots\ModuleSection');

    }


    public function mapping(){
        return $this->hasMany('App\Models\Config\Mapping');
    }


    public static function create(array $attributes = [])
    {

        $attributes = array_merge($attributes, ['uniqueKey' => \Webpatser\Uuid\Uuid::generate(4)->string] );

        return static::query()->create($attributes);

    }



}
