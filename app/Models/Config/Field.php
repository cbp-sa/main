<?php

namespace App\Models\Config;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Field extends Model
{
    use SoftDeletes;

    protected $table = 'section_input_field';

}
