<?php

namespace App\Models\Config;

use App\Helpers\Traits\ModelHelper;
use App\Helpers\Traits\RelationshipsHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Product extends Model
{
    use SoftDeletes;
    use RelationshipsHelper;
    use ModelHelper;

    protected $table = 'product';

    protected $fillable = ['name', 'description', 'icon'];

    public function module(){

        return $this->belongsToMany('App\Models\Config\Module', 'module_product', 'product_id', 'module_id')->using('App\Models\Pivots\ModuleProduct');

    }



}
