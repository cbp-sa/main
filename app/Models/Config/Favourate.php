<?php

namespace App\Models\Config;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Favourate extends Model
{

    public static function boot(){

        parent::boot();

        static::addGlobalScope(function ($query) {
            $query->where('userCode',  Auth::user()->userCode);
        });

    }

    protected $table = 'fav_module';

}
