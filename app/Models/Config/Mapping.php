<?php
namespace App\Models\Config;

use App\Helpers\Traits\RelationshipsHelper;
use Illuminate\Database\Eloquent\Model;


class Mapping extends Model
{
    use RelationshipsHelper;

    protected $table = 'section_mapping';
    protected $fillable = ['name', 'type', 'map', 'section_id'];

}
