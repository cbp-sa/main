<?php

namespace App\Models\Layout;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Navbar extends Model
{
    use SoftDeletes;
    
    protected $table = 'navbar';

}
